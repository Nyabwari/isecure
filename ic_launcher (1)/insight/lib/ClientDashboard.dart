import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


import 'InkWellDrawer.dart';
import 'ProfilePage.dart';

const mainColor = Color(0xff2470c7);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffD3D3D3);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat="-1.04394";
String lon="37.0952399";
String alt=" ";
String town=" ";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";

GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<ActiveAlerts> activeAlerts;
List<InactiveAlerts> inactiveAlerts;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class ClientDashboard extends StatefulWidget {

  _ClientDashboard createState() => _ClientDashboard();

}


class  _ClientDashboard extends State< ClientDashboard> {



  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    initPlatformState();

    firebaseCloudMessaging_Listeners();
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        showNotification(
            message['notification']['title'], message['notification']['body']);
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    usertokeni = prefs.getString('token');
    nem= prefs.getString('SFPName');
    fon= prefs.getString('SFPTelephone');
    emair= prefs.getString('SFPEmailAddress');
    clogo= prefs.getString('CompanyLogo');
    alogo= prefs.getString('AppLogo');
    id= prefs.getString('user_id');

    print("Aye enchogu, id ni "+id);
    _getUsers();
    setState(() {
      setState(() {
        OneSignal.shared.setExternalUserId(id);
        jina;
        lastCheck;
        usertokeni;
        nem;
        emair;
        fon;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _alert() async {

    if(akishoni=="Acknowledge")
    {
      lon=" ";
      lat=" ";
    }
    pr.show();
    print("sasa");
    String apiUrl = "http://alerts.p-count.org/mobile/V2/AcknowledgeAlert";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"id": id, "action": akishoni,"longitude": lon,"latitude": lat});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Reporting succesful!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<List<ActiveAlerts>> _getUsers() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getActiveAlerts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ActiveAlerts> alerts = [];

      if (map['Active_Alerts'] != null) {
        print("sawasawa");
        activeAlerts = new List<ActiveAlerts>();
        map['Active_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/major.png";
          if (v["Category"]=="Minor")
            urll="assets/incident.png";
          if (v["Category"]=="Info")
            urll="assets/minor.png";

          String s= v["PossibleAction"].toString();
          String ss;

          if(s=="[Safe, Unsafe]")
            ss="safe";
          if(s=="[Acknowledge]")
            ss="akno";
          if(s=="[]")
            ss="nothing";



          print(s);

          ActiveAlerts alert = ActiveAlerts(v["id"],v["cartegory"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll, ss,v["alert_title"],v["AlertState"].toString() );

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }
      String active=map['Active_Alerts'];
      print("nimepoteza mia mbili kwa njia "+active);
      print(map['Active_Alerts'].length);



    }
    else {
      print("no");
    }

  }
  Future<List<InactiveAlerts>> _getUserss() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getActiveAlerts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<InactiveAlerts> alerts = [];

      if (map['Inactive_Alerts'] != null) {
        print("sawasawa");
        inactiveAlerts = new List<InactiveAlerts>();
        map['Inactive_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/major.png";
          if (v["Category"]=="Minor")
            urll="assets/incident.png";
          if (v["Category"]=="Info")
            urll="assets/minor.png";



          InactiveAlerts alert = InactiveAlerts(v["id"],v["cartegory"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll,v["alert_title"] );

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }
      String active=map['Active_Alerts'];
      print("nimepoteza mia mbili kwa njia "+active);
      print(map['Active_Alerts'].length);



    }
    else {
      print("no");
    }

  }

  ListView _jobsListView(activeAlerts) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    void handleClick(String value) {
      switch (value) {
        case 'Profile':
          Navigator.push(context,
              new MaterialPageRoute(builder: (ctxt) => new ProfilePage())
          );
          break;
      }
    }
    return Scaffold(
        drawer: InkWellDrawer(),

      appBar: AppBar(
        backgroundColor: mainColor,
        title: Image.network(clogo, fit: BoxFit.contain, height: 47
        ),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return { 'Profile'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
        body: SingleChildScrollView(
          child: Stack(

            children: <Widget>[

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,

                children: <Widget>[
                  Container(height: 10, color: mainColor),
                  Container(height: 8, color: Colors.transparent),
                  Text(
                    jina,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color:mainColor,
                        fontSize: MediaQuery
                            .of(context)
                            .size
                            .height / 45,
                        fontWeight: FontWeight.bold
                    ),

                  ),
                  Container(height: 5, color: Colors.transparent),

                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child:  Text(
                            "Last Seen : ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color:mainColor,

                            ),

                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            child:  Text(
                              formattedDate,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color:mainColor,

                              ),

                            ),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Container(
                            child:  Text(
                              address,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color:mainColor,

                              ),

                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(height: 5, color: Colors.transparent),
                  Container(height: 8, color:listcolor),
                  DefaultTabController(
                    length: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.symmetric(horizontal:5.0),
                          color: listcolor,
                          child: Material(
                            color: listcolor,
                            child:  TabBar(
                                labelColor: Colors.redAccent,
                                unselectedLabelColor: mainColor,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),

                                    color: Colors.white),
                                tabs: [
                                  Tab(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "Active",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: MediaQuery
                                                .of(context)
                                                .size
                                                .height / 35,
                                            fontWeight: FontWeight.bold
                                        ),

                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child:  Text(
                                        "Inactive",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: MediaQuery
                                                .of(context)
                                                .size
                                                .height / 35,
                                            fontWeight: FontWeight.bold
                                        ),

                                      ),
                                    ),
                                  ),
                                ]
                            ),
                          ),
                        ),
                        Container(
                          //Add this to give height
                          height: MediaQuery.of(context).size.height,
                          child: TabBarView(children: [
                            Container(

                              child: FutureBuilder(
                                future: _getUsers(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  } else {
                                    if(snapshot.data.length == 0){
                                      return Container(
                                          child: Center(
                                            child: Text(
                                                "No Active Alerts Found!",
                                                style: TextStyle(color: mainColor)
                                            ),
                                          )
                                      );
                                    }
                                    return ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Card(
                                          margin: EdgeInsets.all(1.6),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          elevation: 2.0,
                                          child: Container(
                                              decoration: BoxDecoration(color: nyeupe,
                                                borderRadius: BorderRadius.circular(40),
                                              ),
                                        child:                   Container(
                                          padding: EdgeInsets.only(bottom:5, top:5, right: 6, left:6),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                                child:  Container(
                                                    padding: EdgeInsets.only(bottom:5, top:5, right: 8, left:6),
                                                    child: Image(image: AssetImage(snapshot.data[index].imgUrl))


                                                ),
                                              ),

                                              Expanded(
                                                flex: 8,
                                                child: Container(
                                                  decoration: new BoxDecoration(
                                                      border: new Border(
                                                          right: new BorderSide(width: 1.5,   color:listcolor),
                                                          left: new BorderSide(width: 1.5,   color:listcolor))),
                                                  child:   Column(
                                                      children: <Widget>[
                                                        Container(

                                                          child: Text(
                                                            snapshot.data[index].alertName,
                                                            style: TextStyle(color: mainColor),
                                                          ),

                                                        ),

                                                     Container(height: 5, color: Colors.transparent),

                                                        (snapshot.data[index].action=="nothing"||snapshot.data[index].shtate=="1")
                                                            ?   Container(height: 3, color: Colors.transparent)

                                                            : Container(
                                                            padding: EdgeInsets.all(0), //<- try add this
                                                            margin: EdgeInsets.only(top: 5.0),
                                                            height: 24,

                                                            child: (snapshot.data[index].action=="safe")
                                                                ? Row(
                                                              children: <Widget>[
                                                                Container(width: 50, color: Colors.transparent),

                                                                Expanded(

                                                                  child:   new RaisedButton(
                                                                    padding: EdgeInsets.all(0), //<- try add this
                                                                    elevation: 3.0,
                                                                    color: Colors.green,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(20.0),
                                                                    ),

                                                                    onPressed: () {
                                                                      id= snapshot.data[index].index.toString();
                                                                      akishoni="Safe";
                                                                      _alert();
                                                                      refreshh;
                                                                    },
                                                                    child: Column(
                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                      children: <Widget>[
                                                                        Text(
                                                                          "Safe",
                                                                          textAlign: TextAlign.center,
                                                                          style: TextStyle(
                                                                            color: Colors.white,
                                                                          ),
                                                                        ),

                                                                      ],
                                                                    ),
                                                                  ),

                                                                ),
                                                                Container(width: 20, color: Colors.transparent),
                                                                Expanded(

                                                                  child:    new RaisedButton(
                                                                    elevation: 3.0,
                                                                    color: Colors.redAccent,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(20.0),
                                                                    ),
                                                                    onPressed: () {
                                                                      id= snapshot.data[index].index.toString();
                                                                      akishoni="UnSafe";
                                                                      _alert();
                                                                    },
                                                                    child: Column(
                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                      children: <Widget>[

                                                                        Text(
                                                                          "Unsafe",
                                                                          textAlign: TextAlign.center,
                                                                          style: TextStyle(
                                                                            color: Colors.white,
                                                                          ),

                                                                        ),

                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(width: 10, color: Colors.transparent),

                                                              ],
                                                            )
                                                                : Container(
                                                              margin: const EdgeInsets.only(left: 65.0, right: 65.0),
                                                              child:    new RaisedButton(
                                                                padding: EdgeInsets.all(0), //<- try add this
                                                                elevation: 3.0,
                                                                color: Colors.redAccent,
                                                                shape: RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.circular(20.0),
                                                                ),
                                                                onPressed: () {
                                                                  id= snapshot.data[index].index.toString();
                                                                  akishoni="Acknowledge";
                                                                  _alert();
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: <Widget>[

                                                                    Center(child: Text(
                                                                      "Acknowledge",
                                                                      style: TextStyle(
                                                                        color: Colors.white,
                                                                      ),

                                                                    ),  ),

                                                                  ],
                                                                ),
                                                              ),

                                                            )
                                                        ),

                                                      ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                  child:                                                          IconButton(
                                                    icon: new Icon(Icons.keyboard_arrow_right),
                                                    highlightColor: Colors.green,
                                                    onPressed: () {
                                                      showDialog(
                                                        context: context,
                                                        builder: (context) {
                                                          return Card(

                                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                            elevation: 16,
                                                            child: Column(
                                                              children: <Widget>[

                                                                Container(
                                                                    height: MediaQuery.of(context).size.height * 0.08,
                                                                    width: MediaQuery.of(context).size.width,
                                                                    child: ColoredBox(
                                                                      color: mainColor,
                                                                      child: Stack(

                                                                          children:<Widget>[
                                                                            Align(
                                                                              alignment: Alignment.centerLeft,
                                                                              child: InkWell(
                                                                                onTap: () {
                                                                                  Navigator.pop(context);
                                                                                },
                                                                                child: Icon(Icons.arrow_back, color: Colors.white),
                                                                              ),
                                                                            ),

                                                                            Align(
                                                                              alignment: Alignment.center,
                                                                              child: Text(snapshot.data[index].heading.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                fontWeight: FontWeight.bold,
                                                                                fontSize: 20,
                                                                                color: Colors.white,
                                                                              ),),
                                                                            ),

                                                                          ]),
                                                                    )
                                                                ),
                                                                Container(height: 4, color: Colors.transparent),

                                                                Stack(

                                                                    children:<Widget>[
                                                                      Align(
                                                                        alignment: Alignment.centerLeft,
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.all(8.0),
                                                                          child: Text(snapshot.data[index].alertTime.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                            fontSize: 18,
                                                                            color: mainColor,
                                                                          ),),
                                                                        ),
                                                                      ),
                                                                      Align(
                                                                        alignment: Alignment.center,
                                                                        child: Padding(
                                                                          padding: const EdgeInsets.all(8.0),
                                                                          child: Text(snapshot.data[index].alertDate.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                            fontSize: 18,
                                                                            color: mainColor,
                                                                          ),),),
                                                                      ),
                                                                      Align(
                                                                        alignment: Alignment.centerRight,
                                                                        child:Container
                                                                          ( height: 30,
                                                                          child: Padding(
                                                                            padding: const EdgeInsets.only(right: 15, top: 3),
                                                                            child: Image(image: AssetImage(snapshot.data[index].imgUrl)
                                                                            ),),),
                                                                      )
                                                                    ]),
                                                                Divider(
                                                                  thickness: 1,
                                                                  color: Color(0xff818181),
                                                                ),
                                                                Container(
                                                                  height: MediaQuery
                                                                      .of(context)
                                                                      .size
                                                                      .height * 0.40,
                                                                  width: MediaQuery
                                                                      .of(context)
                                                                      .size
                                                                      .width * 0.999,

                                                                  child: Card(
                                                                    elevation: 5.0,
                                                                    color: listcolor,
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(30.0),
                                                                    ),
                                                                    child :Padding(
                                                                      padding: const EdgeInsets.all(13.0),
                                                                      child: Text(
                                                                        snapshot.data[index].alertName.toString(),
                                                                        textAlign: TextAlign.center,
                                                                        style: TextStyle(
                                                                          color: mainColor,
                                                                          fontSize: MediaQuery
                                                                              .of(context)
                                                                              .size
                                                                              .height / 40,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(height: 15, color: Colors.transparent),
                                                                (snapshot.data[index].action=="nothing"||snapshot.data[index].shtate=="1")

                                                                    ?   Container(height: 3, color: Colors.transparent)

                                                                    : Container(
                                                                    padding: EdgeInsets.all(0), //<- try add this
                                                                    margin: EdgeInsets.only(top: 5.0),
                                                                    height: 35,

                                                                    child: (snapshot.data[index].action=="safe")
                                                                        ? Row(
                                                                      children: <Widget>[
                                                                        Container(width: 20, color: Colors.transparent),

                                                                        Expanded(

                                                                          child:   new RaisedButton(
                                                                            padding: EdgeInsets.all(0), //<- try add this
                                                                            elevation: 3.0,
                                                                            color: Colors.green,
                                                                            shape: RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(20.0),
                                                                            ),

                                                                            onPressed: () {
                                                                              id= snapshot.data[index].index.toString();
                                                                              akishoni="Safe";
                                                                              _alert();
                                                                            },
                                                                            child: Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  "Safe",
                                                                                  textAlign: TextAlign.center,
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                  ),
                                                                                ),

                                                                              ],
                                                                            ),
                                                                          ),

                                                                        ),
                                                                        Container(width: 20, color: Colors.transparent),
                                                                        Expanded(

                                                                          child:    new RaisedButton(
                                                                            elevation: 3.0,
                                                                            color: Colors.redAccent,
                                                                            shape: RoundedRectangleBorder(
                                                                              borderRadius: BorderRadius.circular(20.0),
                                                                            ),
                                                                            onPressed: () {
                                                                              id= snapshot.data[index].index.toString();
                                                                              akishoni="UnSafe";
                                                                              _alert();
                                                                            },
                                                                            child: Column(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                              children: <Widget>[

                                                                                Text(
                                                                                  "Unsafe",
                                                                                  textAlign: TextAlign.center,
                                                                                  style: TextStyle(
                                                                                    color: Colors.white,
                                                                                  ),

                                                                                ),

                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Container(width: 20, color: Colors.transparent),

                                                                      ],
                                                                    )
                                                                        : Container(
                                                                      margin: const EdgeInsets.only(left: 65.0, right: 65.0),
                                                                      child:    new RaisedButton(
                                                                        padding: EdgeInsets.all(0), //<- try add this
                                                                        elevation: 3.0,
                                                                        color: Colors.redAccent,
                                                                        shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(20.0),
                                                                        ),
                                                                        onPressed: () {
                                                                          id= snapshot.data[index].index.toString();
                                                                          akishoni="Acknowledge";
                                                                          _alert();
                                                                        },
                                                                        child: Column(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          children: <Widget>[

                                                                            Center(child: Text(
                                                                              "Acknowledge",
                                                                              style: TextStyle(
                                                                                color: Colors.white,
                                                                              ),

                                                                            ),  ),

                                                                          ],
                                                                        ),
                                                                      ),

                                                                    )
                                                                ),
                                                                Column(

                                                                  children: <Widget>[
                                                                    Container(height: 50, color: Colors.transparent),


                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          );
                                                        },
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),

                                              )
                                        );
                                      },
                                    );
                                  }
                                },
                              ),


                            ),
                            Container(

                              child: FutureBuilder(
                                future: _getUserss(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  } else {
                                    if(snapshot.data.length == 0){
                                      return Container(
                                          child: Center(
                                            child: Text(
                                                "No Inactive Alerts Found!",
                                                style: TextStyle(color: mainColor)
                                            ),
                                          )
                                      );
                                    }
                                    return ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Card(
                                            margin: EdgeInsets.all(1.6),
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                            elevation: 2.0,
                                            child: Container(
                                              decoration: BoxDecoration(color: nyeupe,
                                                borderRadius: BorderRadius.circular(40),
                                              ),
                                              child:                   Container(
                                                padding: EdgeInsets.only(bottom:5, top:5, right: 6, left:6),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 1,
                                                      child:  Container(
                                                          padding: EdgeInsets.only(bottom:5, top:5, right: 8, left:6),
                                                          child: Image(image: AssetImage(snapshot.data[index].imgUrl))


                                                      ),
                                                    ),

                                                    Expanded(
                                                      flex: 8,
                                                      child: Container(
                                                        decoration: new BoxDecoration(
                                                            border: new Border(
                                                                right: new BorderSide(width: 1.5,   color:listcolor),
                                                                left: new BorderSide(width: 1.5,   color:listcolor))),
                                                        child:   Column(
                                                          children: <Widget>[
                                                            Container(

                                                              child: Text(
                                                                snapshot.data[index].alertName,
                                                                style: TextStyle(color: mainColor),
                                                              ),

                                                            ),

                                                            Container(height: 5, color: Colors.transparent),

                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Container(
                                                        child:  IconButton(
                                                          icon: new Icon(Icons.keyboard_arrow_right),
                                                          highlightColor: Colors.green,
                                                          onPressed: () {
                                                            showDialog(
                                                              context: context,
                                                              builder: (context) {
                                                                return Card(

                                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                  elevation: 16,
                                                                  child: Column(
                                                                    children: <Widget>[

                                                                      Container(
                                                                          height: MediaQuery.of(context).size.height * 0.08,
                                                                          width: MediaQuery.of(context).size.width,
                                                                          child: ColoredBox(
                                                                            color: mainColor,
                                                                            child: Stack(

                                                                                children:<Widget>[
                                                                                  Align(
                                                                                    alignment: Alignment.centerLeft,
                                                                                    child: InkWell(
                                                                                      onTap: () {
                                                                                        Navigator.pop(context);
                                                                                      },
                                                                                      child: Icon(Icons.arrow_back, color: Colors.white),
                                                                                    ),
                                                                                  ),

                                                                                  Align(
                                                                                    alignment: Alignment.center,
                                                                                    child: Text(snapshot.data[index].heading.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                      fontWeight: FontWeight.bold,
                                                                                      fontSize: 20,
                                                                                      color: Colors.white,
                                                                                    ),),
                                                                                  ),

                                                                                ]),
                                                                          )
                                                                      ),
                                                                      Container(height: 4, color: Colors.transparent),

                                                                      Stack(

                                                                          children:<Widget>[
                                                                            Align(
                                                                              alignment: Alignment.centerLeft,
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.all(8.0),
                                                                                child: Text(snapshot.data[index].alertTime.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                  fontSize: 18,
                                                                                  color: mainColor,
                                                                                ),),
                                                                              ),
                                                                            ),
                                                                            Align(
                                                                              alignment: Alignment.center,
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.all(8.0),
                                                                                child: Text(snapshot.data[index].alertDate.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                  fontSize: 18,
                                                                                  color: mainColor,
                                                                                ),),),
                                                                            ),
                                                                            Align(
                                                                              alignment: Alignment.centerRight,
                                                                              child:Container
                                                                                ( height: 30,
                                                                                child: Padding(
                                                                                  padding: const EdgeInsets.only(right: 15, top: 3),
                                                                                  child: Image(image: AssetImage(snapshot.data[index].imgUrl)
                                                                                  ),),),
                                                                            )
                                                                          ]),
                                                                      Divider(
                                                                        thickness: 1,
                                                                        color: Color(0xff818181),
                                                                      ),
                                                                      Container(
                                                                        height: MediaQuery
                                                                            .of(context)
                                                                            .size
                                                                            .height * 0.40,
                                                                        width: MediaQuery
                                                                            .of(context)
                                                                            .size
                                                                            .width * 0.999,

                                                                        child: Card(
                                                                          elevation: 5.0,
                                                                          color: listcolor,
                                                                          shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(30.0),
                                                                          ),
                                                                          child :Padding(
                                                                            padding: const EdgeInsets.all(13.0),
                                                                            child: Text(
                                                                              snapshot.data[index].alertName.toString(),
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(
                                                                                color: mainColor,
                                                                                fontSize: MediaQuery
                                                                                    .of(context)
                                                                                    .size
                                                                                    .height / 40,
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Container(height: 15, color: Colors.transparent),

                                                                      Column(

                                                                        children: <Widget>[
                                                                          Container(height: 50, color: Colors.transparent),


                                                                        ],
                                                                      )
                                                                    ],
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            )
                                        );
                                      },
                                    );
                                  }
                                },
                              ),


                            ),

                          ]),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ],

          ),
        ),
        bottomNavigationBar: Container(
          height: 45.0,
          child: BottomAppBar(
              color: Colors.white,
              child: Container(
                margin: const EdgeInsets.only(left: 50.0, right: 50.0, top: 5, bottom: 5),
                child: RaisedButton(
                  elevation: 5.0,
                  color: nyekundu,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  onPressed: () {


                    showDialog(
                      context: context,
                      builder: (context) {
                        return Card(

                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                          elevation: 16,
                          child: Column(
                            children: <Widget>[

                              Container(
                                  height: MediaQuery.of(context).size.height * 0.08,
                                  width: MediaQuery.of(context).size.width,
                                  child: ColoredBox(
                                    color: mainColor,
                                    child: Stack(

                                        children:<Widget>[
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: InkWell(
                                              onTap: () {
                                                Navigator.pop(context);
                                              },
                                              child: Icon(Icons.arrow_back, color: Colors.white),
                                            ),
                                          ),

                                          Align(
                                            alignment: Alignment.center,
                                            child: Text("Emergency Contacts", textAlign: TextAlign.center, style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20,
                                              color: Colors.white,
                                            ),),
                                          ),

                                        ]),
                                  )
                              ),

                              Column(

                                children: <Widget>[


                        Container(
                          margin: const EdgeInsets.only(right: 8.0, left: 8.0),
                        decoration: BoxDecoration(color: mainColor),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.40,
                        child: Column(
                        children: <Widget>[
                        SizedBox(height: 20),
                        SizedBox(height: 20),
                        Icon(
                        Icons.person,
                        color: Colors.white,
                        size: 160,
                        ),
                        Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                        Padding(
                        padding: const EdgeInsets.all(8),
                        child: Text(
                        nem,
                        style: TextStyle(color: Colors.white, fontSize: 30),
                        ),
                        ),
                        ],
                        ),
                        ],
                        ),
                        ),
                    Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                    children: <Widget>[
                    Card(
                    child: ListTile(
                            title:Text(
                              fon,
                              style: TextStyle(color: Colors.black54),
                            ),
                            subtitle: Text(
                            "SFP Telephone Number",
                            style: TextStyle(color: Colors.black54),
                            ),
                            leading: IconButton(
                            icon: Icon(Icons.phone, color:mainColor),
                            onPressed: () {
                            _launchCaller(fon);
                            },
                            ),
                            trailing: IconButton(
                            icon: Icon(Icons.message),
                            onPressed: () {
                            _textMe(fon);
                            },
                            ),
                            ),
                            ),
                            Card(
                            child: ListTile(
                            title: Text( emair ),
                            subtitle: Text(
                            "SFP Email address",
                            style: TextStyle(color: Colors.black54),
                            ),
                            leading: IconButton(
                            icon: Icon(Icons.email, color: mainColor),
                            onPressed: () {}),
                            ),
                            ),


                            ],
                            ),
                            )

                                ],
                              )
                            ],
                          ),
                        );
                      },
                    );
                  },
                  child: Text(
                    "Emergency Contact",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: MediaQuery.of(context).size.height / 40,
                    ),
                  ),
                ),
              )
          ),
        ),

    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


void firebaseCloudMessaging_Listeners() {
  if (Platform.isIOS) iOS_Permission();

  _firebaseMessaging.getToken().then((token){
    print("antho token ni "+token);
    etokeni=token;
    _sendToken();
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    //  onBackgroundMessage: myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
  );
}


void iOS_Permission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true)
  );
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings)
  {
    print("Settings registered: $settings");
  });
}

Future onSelectNotification(String payload) async {
  showDialog(
    builder: (BuildContext context) {
      return new AlertDialog(
        title: Text("PayLoad"),
        content: Text("Payload : $payload"),
      );
    },
  );
}
void showNotification(String title, String body) async {
  await _demoNotification(title, body);
}

Future<void> _demoNotification(String title, String body) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID', 'channel name', 'channel description',
      importance: Importance.Max,
      playSound: true,
      sound: RawResourceAndroidNotificationSound('alarm.mp3'),
      showProgress: true,
      priority: Priority.High,
      ticker: 'test ticker');

  var iOSChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSChannelSpecifics);
  await flutterLocalNotificationsPlugin
      .show(0, title, body, platformChannelSpecifics, payload: 'test');
}
Future<void> _sendToken() async {
  print("aye enchogu "+ etokeni);
  String apiUrl = "http://alerts.p-count.org/mobile/V2/updateDeviceToken";
  Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
  final json =  convert.jsonEncode({  "DeviceToken": etokeni});
  http.Response response = await http.post(apiUrl,headers: headers, body: json);
  var jsonResponse = convert.jsonDecode(response.body);
  if (response.statusCode == 200) {

    String jsonsDataString = response.body.toString();
    print("jibu ni");
    print(jsonsDataString);

    Map<String, dynamic> map = jsonDecode(jsonsDataString);



  }
  else {
    print("no");
  }
}


class ActiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String action;
  String heading;
  String shtate;


  ActiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.action, this. heading, this. shtate);

}
class InactiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String heading;

  InactiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.heading);
}
Future<void> initPlatformState() async {


  print("Before set log");
  //OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  print("Before set user privacy");
  //OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);
  OneSignal.shared.setExternalUserId(id);

  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  OneSignal.shared.setNotificationReceivedHandler((notification) {


  });

  OneSignal.shared
      .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print("Payload only is"+result.notification.payload.jsonRepresentation().toString());

  });
  OneSignal.shared.setExternalUserId(id);

  OneSignal.shared
      .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
    print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
  });
  OneSignal.shared.setExternalUserId(id);


  // NOTE: Replace with your own app ID from https://www.onesignal.com
  await OneSignal.shared
      .init("d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0", iOSSettings: settings);

  OneSignal.shared.consentGranted(true);
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.getPermissionSubscriptionState();
  bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

}