import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'dart:io';
import 'dart:convert';
import 'dart:io' as Io;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
//import 'package:image_picker/image_picker.dart' as Image ;
import 'package:image_picker/image_picker.dart';


String name="Enchogu ";
String email=" ";
String phoneNo=" ";
String homeLocation=" ";
String workLocation=" ";
const mainColor = Color(0xff2470c7);
String usertokeni = '';
String companyName=" ";
String emergencyContact=" ";
String department=" ";
String companyTelephone=" ";
String position=" ";
String picha=" ";
String img64;
ProgressDialog pr, prr;

File _image;
final picker = ImagePicker();

class ProfilePage extends StatefulWidget {
  @override
  MapScreenState createState() => MapScreenState();
}

class MapScreenState extends State<ProfilePage>
    with SingleTickerProviderStateMixin {
  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _restore();
  }
  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString('name');
    picha = prefs.getString('UserProfile');
    usertokeni = prefs.getString('token');
    email= prefs.getString('email');
    phoneNo= prefs.getString('telephone');
    homeLocation= prefs.getString('home_location');
    workLocation= prefs.getString('work_location');
    companyName= prefs.getString('CompanyName');
    companyTelephone= prefs.getString('CompnayTelephone');
    position= prefs.getString('position');
    emergencyContact= prefs.getString('emergency_contact');
    department= prefs.getString('Department');
    setState(() {
      name;
      usertokeni;
      email;
      phoneNo;
      homeLocation;
      workLocation;
      companyName;
      companyTelephone;
      position;
      print ("Jina"+name);
    });
  }
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Updating Profile...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    return new Scaffold(
        body: new Container(
          color: Colors.white,
          child: new ListView(
            children: <Widget>[
              Column(
                children: <Widget>[

                  new Container(
                    height: 250.0,
                    color: mainColor,
                    child: new Column(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 10.0, top: 20.0),
                            child: new Row(

                              children: <Widget>[
                                new  IconButton(

                                  icon: Icon(Icons.arrow_back_ios, size: 22.0),
                                  color: Colors.white,
                                  onPressed: () => Navigator.of(context).pop(true),

                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 20.0),
                                  child: new Text('PROFILE',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0,
                                          fontFamily: 'sans-serif-light',
                                          color: Colors.white)),
                                )
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: new Stack(fit: StackFit.loose, children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                _image != null
                                    ? ClipRRect(
                                  borderRadius: BorderRadius.circular(70),
                                  child: Image.file(
                                    _image,
                                    width: 140,
                                    height: 140,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )
                                    :
                                Container(
                                    width: 140.0,
                                    height: 140.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,

                                      image: new DecorationImage(
                                        image: NetworkImage(picha),

                                        fit: BoxFit.cover,
                                      ),
                                    )),
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 90.0, right: 100.0),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new CircleAvatar(
                                      backgroundColor: Colors.red,
                                      radius: 25.0,
                                      child:
                                      new IconButton(
                                        icon: new Icon(
                                          Icons.camera_alt,
                                          color: Colors.white,

                                        ),
                                        onPressed: () {  _showPicker(context); },
                                        )

                                    )
                                  ],
                                )),
                          ]),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 25.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Personal Information',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      _status ? _getEditIcon() : new Container(),
                                    ],
                                  )
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Name',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                      decoration:  InputDecoration(
                                        hintText: name,
                                      ),
                                      enabled: !_status,
                                      autofocus: !_status,
                                      onChanged: (value) {
                                        setState(() {
                                          name = value;
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Email ID',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                      decoration: InputDecoration(
                                          hintText:email),
                                      enabled: !_status,
                                      onChanged: (value) {
                                        setState(() {
                                          email = value;
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      new Text(
                                        'Mobile',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  new Flexible(
                                    child: new TextField(
                                      decoration: InputDecoration(
                                          hintText: phoneNo),
                                      enabled: !_status,
                                      onChanged: (value) {
                                        setState(() {
                                          phoneNo = value;
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      child: new Text(
                                        'Home Location',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: new Text(
                                        'Work Location',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 2.0),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                      child: new TextField(
                                        decoration:  InputDecoration(
                                            hintText: homeLocation),
                                        enabled: !_status,
                                        onChanged: (value) {
                                          setState(() {
                                            homeLocation = value;
                                          });
                                        },
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Flexible(
                                    child: new TextField(
                                      decoration: InputDecoration(
                                          hintText: workLocation),
                                      enabled: !_status,
                                      onChanged: (value) {
                                        setState(() {
                                          workLocation = value;
                                        });
                                      },
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              )),


                          !_status ? _getActionButtons() : new Container(),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myFocusNode.dispose();
    super.dispose();
  }
  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50
    );

    setState(() {
      _image = image;
      final bytes = Io.File(_image.path).readAsBytesSync();


      img64 = base64Encode(bytes);
      print(img64);
      print("apra "+usertokeni);

    });
  }

  _imgFromGallery() async {
    File image = await  ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50
    );

    setState(() {
      _image = image;
      final bytes = Io.File(_image.path).readAsBytesSync();

 img64 = base64Encode(bytes);
      print(img64);
    });
  }
  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                    child: new Text("Save"),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () {
                      setState(() {
                        _status = true;
                        _alert();
                       print("sasa antho" +usertokeni);
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                    child: new Text("Cancel"),
                    textColor: Colors.white,
                    color: Colors.red,
                    onPressed: () {
                      setState(() {
                        _status = true;
                        FocusScope.of(context).requestFocus(new FocusNode());
                      });
                    },
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                  )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }
  Future<void> _alert() async {

    pr.show();
    print("sasa");
    _alertt();
    String apiUrl = "http://alerts.p-count.org/mobile/V2/PostProfile";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"name": name, "email": email,"telephone": phoneNo,"Position": position, "HomeLocation": homeLocation,"WorkLocation": workLocation,"EmergencyContact": emergencyContact,"Department": department});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    print("wasee "+email);
    print("wasee "+name);

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("name",name);
      prefs.setString("telephone",phoneNo);
      prefs.setString("email",email);
      prefs.setString("position",position);
      prefs.setString("home_location",homeLocation);
      prefs.setString("work_location",workLocation);
      Fluttertoast.showToast(
          msg: "Profile Succesfully Updated!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.black,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }
  Future<void> _alertt() async {

    print("sasa");
    String apiUrl = "http://alerts.p-count.org/mobile/V2/PostNewAvatar";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"avatar": "data:image/jpeg;base64,"+img64});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    print("wasee "+email);
    print("respooooo "+response.body.toString());
_image=null;
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Map<String, dynamic> json = jsonDecode(jsonsDataString);
print("Arrooo"+json['UserProfile'].toString());

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("UserProfile",json['UserProfile'].toString());


    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }
}