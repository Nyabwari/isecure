import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert' as convert;

import 'ClientDashboard.dart';

String contas="";
String region;
String regionidd;
String contanems;
String contanums;
String usertokeni;
String refreshh="";
String objj=" ";
String conId="";
String rist="";
List<ContactList> contactlists;
List<ContactListt> contactlistss;
String _mySelection;
List<Map> _myJson;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  final String reloadLabel = 'Add Contacts!';
  final String fireLabel = 'Select';
  final Color floatingButtonColor = Colors.green;
  final IconData reloadIcon = Icons.add;
  final IconData fireIcon = Icons.filter_center_focus;
  final  mainColor = Color(0xff2470c7);


  @override
  _MyHomePageState createState() => new _MyHomePageState(
    floatingButtonLabel: this.fireLabel,
    icon: this.fireIcon,
    floatingButtonColor: this.floatingButtonColor,
  );
}

class _MyHomePageState extends State<MyHomePage> {

  bool _isLoading = false;
  bool _isSelectedContactsView = false;
  String floatingButtonLabel;
  Color floatingButtonColor;
  IconData icon;

  _MyHomePageState({
    this.floatingButtonLabel,
    this.icon,
    this.floatingButtonColor,
  });

  @override
  void initState() {
    super.initState();
    _restore();

  }

  Future<void> _alert() async {
    Future.delayed(Duration(milliseconds: 300), () async {
      if(akishoni=="Acknowledge")
      {
        lon=" ";
        lat=" ";
      }
      pr.show();
      print("sasaaa");
      print(contanems);
      print(_mySelection);
      print(contanums);
      print(usertokeni);
      String apiUrl = "http://alerts.syve.co.ke/mobile/V2/AddContact";
      Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
      final json =  convert.jsonEncode({"names": contanems, "Status":"Active", "regions": _mySelection, "telephone" : contanums});
      http.Response response = await http.post(apiUrl,headers: headers, body: json);
      String jsonsDataString = response.body.toString();
      print(jsonsDataString);

      if (response.statusCode == 200) {
        pr.hide().then((isHidden) {
          print(isHidden);
        });


        print(contanems);
        print(_mySelection);
        print(contanums);
        print(usertokeni);
        print(jsonsDataString);

        print("ooooo");
        pr.hide().then((isHidden) {
          print(isHidden);
        });
        setState(() { refreshh; });
        Fluttertoast.showToast(
            msg: "Contacts added succesful!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            fontSize: 16.0
        );

      }
      else {
        pr.hide().then((isHidden) {
          print(isHidden);
        });
        print("no");
      }    });

  }



  Future <String> _restore() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("orooo");
    objj= prefs.getString('objj');
    usertokeni= prefs.getString('token');
    String jsonsDataString = objj;
    print(jsonsDataString);

    Map<dynamic, dynamic> map = jsonDecode(jsonsDataString);
    print(map['regions_attached_to'].toString());
    print("Finare");

    setState(() {
      setState(() {
        usertokeni;
      });
    });
    return "nonini";
  }


  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 50,
            backgroundColor: mainColor,
            elevation: 0,
            title: Text(
              "My Contacts",
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,

          ),
          body: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: <Widget>[
//                    FutureBuilder(
//                      future:  _getContacts(),
//                      builder: (BuildContext context, AsyncSnapshot snapshot){
//                        print("Ara");
//
//                        print(snapshot.data);
//                        if(snapshot.data != null){
//                          Map _mapCompany = jsonDecode(objj);
//                          List list = _mapCompany["regions_attached_to"];
//                          return DropdownButton(
//                            hint: new Text(
//                              'Regions Assigned',
//                              style: TextStyle(fontFamily: "Gotham"),
//                            ),
//                            items:list.map((_mapCompany) {
//                              return new DropdownMenuItem(
//                                child: new Text(_mapCompany["region_name"]),
//                                value: _mapCompany["region_id"].toString(),
//                              );
//                            }).toList(),
//                            onChanged: (newVal) {
//                              setState(() {
//                                _mySelection = newVal;
//                                print("onchwoto"+_mySelection );
//                              });
//                            },
//                            value: _mySelection,
//                          );
//                        }
//                        if(snapshot.data == null){
//                          return Text("No Attached Region FoundData");
//                        }
//                        return Container();
//                      },
//                    ),

                    Container(height: 5, color: Colors.transparent),
                    Container(
                      height: MediaQuery.of(context).size.height*0.8,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: FutureBuilder(
                        future: _getConta(),
                        builder: (BuildContext context, AsyncSnapshot snapshot){
                          print(snapshot.data);
                          if(snapshot.data == null){
                            return Container(
                                child: Center(
                                    child: Text("Loading Contacts...")
                                )
                            );
                          } else {
                            return ListView.builder(
                              padding: EdgeInsets.only(bottom:300, top:5),
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Stack(
                                  children: <Widget>[
                                    Card(
                                      child: ListTile(
                                        contentPadding: EdgeInsets.only(left: 0.0, right: 50.0),
                                        title:Text(
                                          snapshot.data[index].name+refreshh,
                                          style: TextStyle(color: Colors.black54),
                                        ),
                                        subtitle: Text(
                                          snapshot.data[index].telephone,
                                          style: TextStyle(color: Colors.black54),
                                        ),
                                        leading: IconButton(
                                          icon: Icon(Icons.phone, color: Colors.green,),
                                          onPressed: () {
                                            _launchCaller(snapshot.data[index].telephone);
                                          },
                                        ),
                                        trailing: IconButton(
                                          icon: Icon(Icons.message),
                                          color: Colors.blue,
                                          onPressed: () {
                                            refreshh;
                                            _textMe(snapshot.data[index].telephone);
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            );
                          }
                        },
                      ),
                    ),


                  ],
                )

            ),

        )
    );
  }
  showAlertDialog(BuildContext context, String message, String heading,
      String buttonAcceptTitle, String buttonCancelTitle) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(buttonCancelTitle),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(buttonAcceptTitle),
      onPressed: () {
        _delete();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(heading),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Future<void> _delete() async {


    pr.show();
    print("sasa");
    String apiUrl = "http://alerts.syve.co.ke/mobile/V2/DeleteContact";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"contact_id": conId});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Navigator.pop(context);
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Contact Deleted Succesfully!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: mainColor,
          textColor: Colors.white,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }


  }


  Future<List<ContactListt>> _getConta() async {
    print("nimefika hapa");
    String apiUrl = "http://alerts.p-count.org/mobile/V2/getContactList";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    http.Response response = await http.get(apiUrl,headers: headers);
    String jsonsDataString = response.body.toString();

    print("nimefika hapa "+jsonsDataString);
    if (response.statusCode == 200) {

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ContactListt> contactListt = [];

      if (map['ContactList'] != null) {
        print("sawasawa");
        print(map['ContactList']);
        contactlistss= new List<ContactListt>();
        map['ContactList'].forEach((v) {
          print("Aleluya123");
          String tele=v["telephone"].toString();
          if(v["telephone"].toString()=="null")
            {
              tele="N/A";
            }
          ContactListt alert = ContactListt( v["contact_id"].toString(), v["name"].toString(), tele, "sasa" );

          contactListt.add(alert);
          print("Aleluya");

        });
        print(contactListt.length);
        return contactListt;

      }


    }
    else {
      print("no");
    }

  }

  Future<List<ContactList>> _getContacts() async {


    String jsonsDataString = objj;
    print(jsonsDataString);

    Map<String, dynamic> map = jsonDecode(jsonsDataString);

    List<ContactList> contactlistss = [];

    if (map['regions_attached_to'] != null) {
      contactlists = new List<ContactList>();
      map['regions_attached_to'].forEach((v) {

        print("contacst sasasa");

        ContactList contactlist = ContactList(
            v["region_id"].toString(),
            v["region_name"].toString()
        );
        print( v["region_id"]);

        contactlistss.add(contactlist);

      });
      print(contactlistss.length);
//        setState(() {
//          shafe="Safe ("+contactlistss.length.toString()+")";
//        });

    }
    return contactlistss;


  }


class ContactList {

  String regid;
  String region;


  ContactList(this.regid, this.region);

}

class ContactListt {

  String contactId;
  String name;
  String telephone;
  String region;

  ContactListt( this.contactId, this.name, this.telephone, this.region);
}
