import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


import 'dart:convert';

import 'ClientDashboard.dart';
import 'dart:io';

import 'SFDashBoard.dart';

String os = Platform.operatingSystem;
String user, jina, etoken=" ";
ProgressDialog pr;
String role="0";


FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
const mainColor = Color(0xff2470c7);
const mainColorr = Color(0xff2e2e2e);
const nyekundu = Color(0xffe82224);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffD3D3D3);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  jina = prefs.getString('user_id');
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.init(
      "d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0",

      iOSSettings: {
        OSiOSSettings.autoPrompt: false,
        OSiOSSettings.inAppLaunchUrl: false
      }
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);

  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('user_id');
    role= prefs.getString('isSFP');
    print(jina);
    if(jina!=null)
      return false;
    else
      return false;
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 6)),
      builder: (context, AsyncSnapshot snapshot) {
        // Show splash screen while waiting for app resources to load:
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MaterialApp(home: Splash());
        } else {
          // Loading is done, return the app:
          return MaterialApp(
              home: FutureBuilder(
                future: isLoggedIn(),
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    return snapshot.data ? ((role=="1")?SFDashBoard(): ClientDashboard()) : LoginPage() ;
                  }
                  return Container(); // noop, this builder is called again when the future completes
                },
              )
          );
        }
      },
    );
  }
}


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  String email, password;
  String username;
  String userId;
  int companyId;
  String companyName;
  List<Null> emergencyContacts;
  String latestMessage;
  String iconUrl;
  bool isAuthenticated;
  String authMessage;
  final _text = TextEditingController();
  bool _validate = false;
  final _formKey = GlobalKey<FormState>();
  final _formKeyy = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();

  }


  Widget _buildLogo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,

      children: <Widget>[
        new
        Expanded(
            child: Column(children: <Widget>[
            Container(
            width: 120.0,
            child: FittedBox(
            fit: BoxFit.contain,
            child:Container(
                height: 120.0,
                width: 120.0,
                child: Padding(
                    padding: EdgeInsets.all(1),
                    child: CircleAvatar(
                      backgroundColor: mainColorr,
                      radius: 10,
                      child: new Image.asset('assets/insightg.jpg'),
                    )),
                decoration: new BoxDecoration(
                  color:Color(0xffffbfbfd),
                  shape: BoxShape.circle,
                  border: new Border.all(
                    color:mainColor,
                    width: 2.0,
                  ),
                ))),),


              Container(
                width: 120.0,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "i",
                          style: TextStyle(color: Colors.red,
                            fontFamily: 'Microsoft Himalaya',
                          )),
                      TextSpan(
                          text: "Secure",
                          style: TextStyle(
                              color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                          )),

                    ]),
                  )

                ),
              ),


            ],)
        ),

      ],
    );
  }

  Future<void> _doSignIn() async {
    print(email);
    print(password);
    print(os);
    print(etoken);
    pr.show();
    String apiUrl = "http://alerts.p-count.org/mobile/V2/userlogin";
    Map<String, String> headers = {"Content-type": "application/json"};
    final json =  convert.jsonEncode({"email": email, "password": password,"DeviceType": os,"DeviceToken": etoken});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });



      Map<String, dynamic> json = jsonDecode(jsonsDataString);

      Map<String, dynamic> map=(json['basic_details']);
      Map<String, dynamic> mapp=(json['EmegencyDetails']);
      print("idd ni "+jsonsDataString);

      print("idd ni "+map.toString());
      if(map==null)
      {
        Fluttertoast.showToast(
            msg: "Incorrect Username Or Password",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }


      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("user_id",map['user_id'].toString());
      prefs.setString("token", map['token'].toString());
      prefs.setString("name",map['name'].toString());
      prefs.setString("telephone",map['telephone'].toString());
      prefs.setString("UserProfile",map['UserProfile'].toString());
      prefs.setString("email",map['email'].toString());
      prefs.setString("position",map['position'].toString());
      prefs.setString("Department",map['Department'].toString());
      prefs.setString("dob",map['dob'].toString());
      prefs.setString("home_location",map['home_location'].toString());
      prefs.setString("work_location",map['work_location'].toString());
      prefs.setString("emergency_contact",map['emergency_contact'].toString());
      prefs.setString("gender",map['gender'].toString());
      prefs.setString("position",map['position'].toString());
      prefs.setString("ip_id",map['ip_id'].toString());
      prefs.setString("isSFP",map['isSFP'].toString());
      prefs.setString("Role",map['Role'].toString());
      prefs.setString("LastSeenDate",map['LastSeenDate'].toString());
      prefs.setString("LastSeenLocation",map['LastSeenLocation'].toString());
      prefs.setString("CompanyName",map['CompanyName'].toString());
      prefs.setString("CompnayTelephone",map['CompnayTelephone'].toString());

      prefs.setString("SFPName",mapp['SFPName'].toString());
      prefs.setString("SFPTelephone",mapp['SFPTelephone'].toString());
      prefs.setString("SFPEmailAddress",mapp['SFPEmailAddress'].toString());

      prefs.setString("CompanyLogo",map['CompanyLogo'].toString());
      prefs.setString("AppLogo",map['AppLogo'].toString());

      print("print tok ni "+map['SFPEmailAddress'].toString());
      print(map['CompanyLogo'].toString());
      print(map['email'].toString());

    print(map['isSFP']);
      if(map['isSFP']==0)
        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => new ClientDashboard())
        );
      else
        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => new SFDashBoard())
        );
    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Fluttertoast.showToast(
          msg: "Incorrect Username Or Password",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      print("nooo");
    }
  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Logging in...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    return Scaffold(
      backgroundColor: Color(0xff2470c7),

      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: IntrinsicHeight(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                // CONTENT HERE
                Spacer(),
                _buildLogo(),
                Container(height: 30, color: Colors.transparent),
                _buildContainer(),
                Container(height: 10, color: Colors.transparent),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget _buildEmailRow() {
    return Padding(
      key: _formKey,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          onChanged: (value) {
            setState(() {
             email = value;
            });
          },
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.person,
            color: mainColor,
          ),
          contentPadding: EdgeInsets.all(0),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(15.0),


            ),

          ),
          labelText: "Email",
        ),
      ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
    ),
    );
  }

  Widget _buildPasswordRow() {
    return Padding(
      key: _formKeyy,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          obscureText: true,
      onChanged: (value) {
          setState(() {
            password = value;
          });
      },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: mainColor,
            ),
            contentPadding: EdgeInsets.all(0),
            border: new OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(15.0),
              ),
            ),
            labelText: "Password",
          ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _buildForgetPasswordButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        FlatButton(
          onPressed: () {},
          child: Text("Forgot Password"),
        ),
      ],
    );
  }

  Widget _buildLoginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 1.4 * (MediaQuery.of(context).size.height / 25),
          width: 6.5 * (MediaQuery.of(context).size.width / 10),
          margin: EdgeInsets.only(bottom: 20),
          child: RaisedButton(
            elevation: 5.0,
            color: nyekundu,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            onPressed: () {
              _doSignIn();
            },
            child: Text(
              "Log In",
              style: TextStyle(
                color: Colors.white,
                fontSize: MediaQuery.of(context).size.height / 40,
              ),
            ),
          ),
        )
      ],
    );
  }
  Widget _buildContainer() {
    _firebaseMessaging.getToken().then((token){
      print("token ni "+token);
    });

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.55,
            width: MediaQuery.of(context).size.width * 0.92,
            decoration: BoxDecoration(
              color: nyeupe,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Log In",
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.height / 20,
                              color: mainColorr,
                              fontFamily: 'Microsoft Himalaya',
                            )),

                      ]),
                    ),
                  ],
                ),
                Container(height: 30, color: Colors.transparent),

                _buildEmailRow(),
                Container(height: 30, color: Colors.transparent),
                _buildPasswordRow(),
                Container(height: 40, color: Colors.transparent),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Forgot Password",
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.height / 40,
                      ),
                    ),
                  ],
                ),
                Container(height: 10, color: Colors.transparent),
                _buildLoginButton(),

              ],
            ),
          ),
        ),
      ],
    );
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token){
      etoken=token;
      print("token ni "+token);
      print("os ni "+os);

    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }
}


class Splash extends StatelessWidget {
  Widget _buildLogoo() {
    return Row(

      children: <Widget>[
        new
        Expanded(
            child: Column(
              mainAxisAlignment : MainAxisAlignment.center,
              crossAxisAlignment : CrossAxisAlignment.center,
              children: <Widget>[
              Container(

                width: 150.0,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child:Image.asset(
                    'assets/insightg.jpg',
                  ),),
              ),
              Container(height: 20, color: Colors.transparent),

              Container(
                width: 120.0,
                child: FittedBox(
                    fit: BoxFit.contain,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Loading",
                            style: TextStyle(color: Colors.white,
                            )),
                        TextSpan(
                            text: "...",
                            style: TextStyle(
                              color: Colors.white,
                            )),

                      ]),
                    )

                ),
              ),


            ],)
        ),

      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,

      body: Center(
        child: _buildLogoo(),
      ),
    );
  }
}



