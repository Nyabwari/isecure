import 'dart:async';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

import 'InkWellDrawer.dart';
import 'MyHomePage.dart';

const mainColor = Color(0xff2470c7);
const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const listcolor = Color(0xffD3D3D3);
var scolor = Color(0xffD3D3D3);
const nyeupe = Color(0xffffffff);


String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat="-1.04394";
String lon="37.0952399";
String alt=" ";
String town=" ";
String shafe="Safe";
String unshafe="Unsafe";
String pendin="Pending";
String acti="Unsafe";
String inacti="Pending";
String akishoni=" ";
String street=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String refreshh="0";
String messo=" ";
String alert_Idd;
String mesho="No updates found in the last four hours";
String  company=" ";
String usertokeni = '';
GoogleMapController controller;
BitmapDescriptor customIcon;
int hesabu=0;
Set<Marker> markers;
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<ActiveAlerts> activeAlerts;
List<InactiveAlerts> inactiveAlerts;
List<ContactList> contactlists;
List<ContactListt> contactlistss;
List<ContactListt> contactlistsss;
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";


FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class SFDashBoard extends StatefulWidget {

  _SFDashBoard createState() => _SFDashBoard();

}


class  _SFDashBoard extends State< SFDashBoard> {



  @override
initState() {
    super.initState();
    _restore();
    _getUsers();

    var initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        showNotification(
            message['notification']['title'], message['notification']['body']);
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));

  }



  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    usertokeni = prefs.getString('token');
    company = prefs.getString('CompanyName');
    clogo= prefs.getString('CompanyLogo');
    alogo= prefs.getString('AppLogo');
    id= prefs.getString('user_id');
    print(id);
    _getUsers();
    initPlatformState();
    setState(() {
      setState(() {
        OneSignal.shared.setExternalUserId(id);

        clogo;
        alogo;
        jina;
        lastCheck;
        usertokeni;
      });
    });
  }


  Future<List<ActiveAlerts>> _getUsers() async {
    String apiUrl = "http://alerts.p-count.org/mobile/V2/getSFPAlertsList";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ActiveAlerts> alerts = [];

      if (map['Active_Alerts'] != null) {
        print("sawasawa");
        activeAlerts = new List<ActiveAlerts>();
        map['Active_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/major.png";
          if (v["Category"]=="Minor")
            urll="assets/incident.png";
          if (v["Category"]=="Info")
            urll="assets/minor.png";


//          String str1 = v["Category"].toString();
//
//          String start = ':';
//          // from index 6 to the last index
//
//          final startIndex = str1.indexOf(start)+2;
//          final startIndexx = str1.indexOf(start);
//          final endIndex = str1.substring(0, startIndexx);
//
//          print(endIndex);
//          print(str1.substring(startIndex ));
          print("richwati eri "+v["AcknowledgementStatus"]);


          ActiveAlerts alert = ActiveAlerts(v["id"],v["Category"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll, v["AlertID"].toString(), v["Total"].toString(), v["Viewed"].toString(), v["NotViewed"].toString(),v["alert_title"],v["AcknowledgementID"].toString(),v["AcknowledgementStatus"].toString());

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }




    }
    else {
      print("no");
    }

  }
  Future<List<InactiveAlerts>> _getUserss() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getSFPAlertsList";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<InactiveAlerts> alerts = [];

      if (map['Inactive_Alerts'] != null) {
        print("sawasawa");
        inactiveAlerts = new List<InactiveAlerts>();
        map['Inactive_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/major.png";
          if (v["Category"]=="Minor")
            urll="assets/incident.png";
          if (v["Category"]=="Info")
            urll="assets/minor.png";


//          String str1 = v["alert_name"].toString();
//
//          String start = ':';
//          // from index 6 to the last index
//          print(str1);
//          final startIndex = str1.indexOf(start)+2;
//          final startIndexx = str1.indexOf(start);
//          final endIndex = str1.substring(0, startIndexx);
//
//          print(endIndex);
//          print(str1.substring(startIndex ));


          InactiveAlerts alert = InactiveAlerts(v["id"],v["Category"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll, v["AlertID"].toString(), v["Total"].toString(), v["Viewed"].toString(), v["NotViewed"].toString(),v["alert_title"]);

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }



    }
    else {
      print("no");
    }

  }

  Future<List<ContactList>> _getList() async {
    String apiUrl = "http://alerts.p-count.org/mobile/V2/getAlertContacts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "alert_id": alert_Idd});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();
      print(jsonsDataString);

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ContactList> contactlistss = [];

      if (map['Contact_list'] != null) {
        contactlists = new List<ContactList>();
        map['Contact_list'].forEach((v) {

          print("contacst sasasa");

          if (v["contact_alert_status"]=="UnSafe")
            scolor=Colors.red;
          if (v["contact_alert_status"]=="Safe")
            scolor=Colors.green;
          if (v["contact_alert_status"]=="Pending")
            scolor=mainColor;
          print(v["name"]);

          if (v["contact_alert_status"]=="Safe") {
            ContactList contactlist = ContactList(
                v["alert_id"].toString(),
                v["contact_id"].toString(),
                v["name"],
                v["telephone"].toString(),
                v["email"],
                v["contact_alert_status"],
                "null");
            print("200 bob imepotea");

            contactlistss.add(contactlist);
          }
        });
        print(contactlistss.length);
//        setState(() {
//          shafe="Safe ("+contactlistss.length.toString()+")";
//        });
        return contactlistss;

      }



    }
    else {
      print("no");
    }

  }
  Future<List<ContactListt>> _getListt() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getAlertContacts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "alert_id": alert_Idd});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();
      print(jsonsDataString);

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ContactListt> contactlistss = [];

      if (map['Contact_list'] != null) {
        contactlists = new List<ContactList>();
        map['Contact_list'].forEach((v) {

          print("contacst sasasa");

          if (v["contact_alert_status"]=="UnSafe")
            scolor=Colors.red;
          if (v["contact_alert_status"]=="Safe")
            scolor=Colors.green;
          if (v["contact_alert_status"]=="Pending")
            scolor=mainColor;
          print(v["name"]);

          if (v["contact_alert_status"]=="UnSafe") {
            ContactListt contactlist = ContactListt(
                v["alert_id"].toString(),
                v["contact_id"].toString(),
                v["name"],
                v["telephone"].toString(),
                v["email"],
                v["contact_alert_status"],
                "null");
            print("200 bob imepotea");

            contactlistss.add(contactlist);
          }
        });
        print(contactlistss.length);
//        setState(() {
//          unshafe="Unsafe ("+contactlistss.length.toString()+")";
//        });
        return contactlistss;

      }



    }
    else {
      print("no");
    }

  }
  Future<List<ContactListtt>> _getListtt() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getAlertContacts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "alert_id": alert_Idd});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();
      print(jsonsDataString);

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ContactListtt> contactlistsss = [];

      if (map['Contact_list'] != null) {
        contactlistsss = new List<ContactListtt>();
        map['Contact_list'].forEach((v) {
          print("contacst sasasa");

          if (v["contact_alert_status"] == "UnSafe")
            scolor = Colors.red;
          if (v["contact_alert_status"] == "Safe")
            scolor = Colors.green;
          if (v["contact_alert_status"] == "Pending")
            scolor = mainColor;
          print(v["name"]);

          if (v["contact_alert_status"] == "Pending") {

          ContactListtt contactlisttt = ContactListtt(
              v["alert_id"].toString(),
              v["contact_id"].toString(),
              v["name"],
              v["telephone"].toString(),
              v["email"],
              v["contact_alert_status"],
              "null");
          print("200 bob imepotea");

          contactlistsss.add(contactlisttt);
        }
        });
        print(contactlistsss.length);
//        setState(() {
//          pendin="Pending ("+contactlistsss.length.toString()+")";
//        });

        return contactlistsss;

      }



    }
    else {
      print("no");
    }

  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    void handleClick(String value) {
      switch (value) {
        case 'Contact List':
          Navigator.push(context,
              new MaterialPageRoute(builder: (ctxt) => new MyHomePage())
          );
          break;
      }
    }
    return Scaffold(
      drawer: InkWellDrawer(),
      appBar: AppBar(
        backgroundColor: mainColor,
        title: Image.network(clogo, fit: BoxFit.contain, height: 50
        ),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return { 'Contact List'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Stack(

          children: <Widget>[

            Column(
              crossAxisAlignment: CrossAxisAlignment.center,

              children: <Widget>[

                Container(height: 10, color: mainColor),
                Container(height: 8, color: Colors.transparent),

                Text(
                  jina,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color:mainColor,
                      fontSize: MediaQuery
                          .of(context)
                          .size
                          .height / 40,
                      fontWeight: FontWeight.bold
                  ),

                ),
                Container(height: 5, color: Colors.transparent),
                Text(
                  company,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color:mainColor,
                  ),

                ),

                Container(height: 5, color: Colors.transparent),
                Container(height: 8, color:listcolor),
                DefaultTabController(
                  length: 2,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        color: listcolor,
                        padding: EdgeInsets.symmetric(horizontal:5.0),

                        child: Material(
                          color: listcolor,
                          child: TabBar(
                              labelColor: Colors.redAccent,
                              unselectedLabelColor: mainColor,
                              indicator: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10)),
                                  color: Colors.white),
                              tabs: [
                                Tab(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Active",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: MediaQuery
                                              .of(context)
                                              .size
                                              .height / 35,
                                          fontWeight: FontWeight.bold
                                      ),

                                    ),
                                  ),
                                ),
                                Tab(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child:  Text(
                                      "Inactive",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: MediaQuery
                                              .of(context)
                                              .size
                                              .height / 35,
                                          fontWeight: FontWeight.bold
                                      ),

                                    ),
                                  ),
                                ),
                              ]
                          ),
                        ),
                        ),

                      Container(
                        //Add this to give height
                        height: MediaQuery.of(context).size.height,
                        child: TabBarView(children: [
                          Container(
                            child: FutureBuilder(
                              future: _getUsers(),
                              builder: (BuildContext context, AsyncSnapshot snapshot){
                                print(snapshot.data);
                                if(snapshot.data == null){
                                  return Container(
                                      child: Center(
                                          child: CircularProgressIndicator()
                                      )
                                  );
                                } else {
                                  if(snapshot.data.length == 0){
                                    return Container(
                                        child: Center(
                                          child: Text(
                                              "No Active Alerts Found!",
                                              style: TextStyle(color: mainColor)
                                          ),
                                        )
                                    );
                                  }
                                  return ListView.builder(
                                    padding: EdgeInsets.only(bottom:300, top:5),
                                   itemCount: snapshot.data.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return Card(
                                          margin: EdgeInsets.all(1.6),
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                          elevation: 2.0,
                                          child: Container(
                                            decoration: BoxDecoration(color: nyeupe,
                                              borderRadius: BorderRadius.circular(40),
                                            ),
                                            child:                   Container(
                                              padding: EdgeInsets.only(bottom:5, top:12, right: 6, left:6),
                                              child: Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child:  Container(
                                                        padding: EdgeInsets.only(bottom:5, top:5, right: 8, left:6),
                                                        child: Image(image: AssetImage(snapshot.data[index].imgUrl))
                                                    ),
                                                  ),

                                                  Expanded(
                                                    flex: 8,
                                                    child: Container(
                                                      decoration: new BoxDecoration(
                                                          border: new Border(
                                                              right: new BorderSide(width: 1.5,   color:listcolor),
                                                              left: new BorderSide(width: 1.5,   color:listcolor))),
                                                      child:   Column(
                                                        children: <Widget>[
                                                          Container(

                                                            child: Text(
                                                              snapshot.data[index].alertName,
                                                              style: TextStyle(color: mainColor),
                                                            ),

                                                          ),

                                                          Container(height: 5, color: Colors.transparent),

                                                          Container(
                                                            padding: EdgeInsets.only(right: 13.0, left:13.0, top:8.0),
                                                            child: Row(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: <Widget>[
                                                                Spacer(),
                                                              Expanded(
                                                                  flex: 3,
                                                                  child:  RichText(
                                                                    text: TextSpan(
                                                                      children: <TextSpan>[
                                                                        TextSpan(text: 'Total  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                                        TextSpan(text:  snapshot.data[index].total.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                                      ],
                                                                    ),
                                                                  )
                                                              ),
                                                              Expanded(
                                                                flex: 3,
                                                                child: Container(
                                                                    child:  RichText(
                                                                      text: TextSpan(
                                                                        children: <TextSpan>[
                                                                          TextSpan(text: 'Viewed  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                                          TextSpan(text: snapshot.data[index].viewed.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                                        ],
                                                                      ),
                                                                    )
                                                                ),
                                                              ),
                                                              Expanded(
                                                                flex: 4,
                                                                child: Container(
                                                                    child:  RichText(
                                                                      text: TextSpan(
                                                                        children: <TextSpan>[
                                                                          TextSpan(text: 'Not Viewed  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                                          TextSpan(text:  snapshot.data[index].notViewed.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                                        ],
                                                                      ),
                                                                    )
                                                                ),
                                                              ),
                                                            Spacer(),
                                                            ],
                                                          ),
                                                          ),
                                                          (snapshot.data[index].AcknowledgementStatus=="Acknowledged")
                                                              ?   Container(height: 1, color: Colors.transparent)

                                                              :
                                                          Container(
                                                            height:32,



                                                            child: Container(
                                                              margin: const EdgeInsets.only(left: 65.0, top:10, right: 65.0),
                                                              child:new RaisedButton(
                                                                padding: EdgeInsets.all(0), //<- try add this
                                                                elevation: 3.0,
                                                                color: Colors.redAccent,
                                                                shape: RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.circular(20.0),
                                                                ),
                                                                onPressed: () {
                                                                  id= snapshot.data[index].AcknowledgementID.toString();
                                                                  akishoni="Acknowledge";
                                                                  _alert();
                                                                },
                                                                child: Column(
                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                  children: <Widget>[

                                                                    Center(child: Text(
                                                                      "Acknowledge",
                                                                      style: TextStyle(
                                                                        color: Colors.white,
                                                                      ),

                                                                    ),  ),

                                                                  ],
                                                                ),
                                                              ),
                                                            ),

                                                          ),

                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Container(
                                                      child: IconButton(
                                                        icon: new Icon(Icons.keyboard_arrow_right),
                                                        highlightColor: Colors.green,
                                                        onPressed: () {
                                                          showDialog(
                                                            context: context,
                                                            builder: (context) {
                                                              return Card(

                                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                elevation: 16,
                                                                child: Column(
                                                                  children: <Widget>[

                                                                    Container(
                                                                        height: MediaQuery.of(context).size.height * 0.08,
                                                                        width: MediaQuery.of(context).size.width,
                                                                        child: ColoredBox(
                                                                          color: mainColor,
                                                                          child: Stack(

                                                                              children:<Widget>[
                                                                                Align(
                                                                                  alignment: Alignment.centerLeft,
                                                                                  child: InkWell(
                                                                                    onTap: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                    child: Icon(Icons.arrow_back, color: Colors.white),
                                                                                  ),
                                                                                ),

                                                                                Align(
                                                                                  alignment: Alignment.center,
                                                                                  child: Text(snapshot.data[index].heading, textAlign: TextAlign.center, style: TextStyle(
                                                                                    fontWeight: FontWeight.bold,
                                                                                    fontSize: 20,
                                                                                    color: Colors.white,
                                                                                  ),),
                                                                                ),

                                                                              ]),
                                                                        )
                                                                    ),
                                                                    Container(height: 4, color: Colors.transparent),

                                                                    Stack(

                                                                        children:<Widget>[
                                                                          Align(
                                                                            alignment: Alignment.centerLeft,
                                                                            child: Padding(
                                                                              padding: const EdgeInsets.all(8.0),
                                                                              child: Text(snapshot.data[index].alertTime.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                fontSize: 18,
                                                                                color: mainColor,
                                                                              ),),
                                                                            ),
                                                                          ),
                                                                          Align(
                                                                            alignment: Alignment.center,
                                                                            child: Padding(
                                                                              padding: const EdgeInsets.all(8.0),
                                                                              child: Text(snapshot.data[index].alertDate.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                                fontSize: 18,
                                                                                color: mainColor,
                                                                              ),),),
                                                                          ),
                                                                          Align(
                                                                            alignment: Alignment.centerRight,
                                                                            child:Container
                                                                              ( height: 30,
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.only(right: 15, top: 3),
                                                                                child: Image(image: AssetImage(snapshot.data[index].imgUrl)
                                                                                ),),),
                                                                          )
                                                                        ]),
                                                                    Divider(
                                                                      thickness: 1,
                                                                      color: Color(0xff818181),
                                                                    ),
                                                                    Container(
                                                                      height: MediaQuery
                                                                          .of(context)
                                                                          .size
                                                                          .height * 0.40,
                                                                      width: MediaQuery
                                                                          .of(context)
                                                                          .size
                                                                          .width * 0.999,

                                                                      child: Card(
                                                                        elevation: 5.0,
                                                                        color: listcolor,
                                                                        shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(30.0),
                                                                        ),
                                                                        child :Padding(
                                                                          padding: const EdgeInsets.all(13.0),
                                                                          child: Text(
                                                                            snapshot.data[index].alertName.toString(),
                                                                            textAlign: TextAlign.center,
                                                                            style: TextStyle(
                                                                              color: mainColor,
                                                                              fontSize: MediaQuery
                                                                                  .of(context)
                                                                                  .size
                                                                                  .height / 40,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(height: 15, color: Colors.transparent),

                                                                    Column(

                                                                      children: <Widget>[
                                                                        Container(height: 20, color: Colors.transparent),
                                                                        (snapshot.data[index].category=="Info"||snapshot.data[index].category=="Minor")

                                                                            ?
                                                                        Container(height: 0, color: Colors.transparent)
                                                                            :
                                                                        Container(
                                                                            width: MediaQuery.of(context).size.width * 0.65, // Will take 50% of screen space
                                                                            child:  RaisedButton(
                                                                              elevation: 5.0,
                                                                              color: nyekundu,
                                                                              shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(15.0),
                                                                              ),
                                                                              onPressed: () {
                                                                                alert_Idd= snapshot.data[index].alertId.toString();


                                                                                showDialog(
                                                                                  context: context,
                                                                                  builder: (context) {
                                                                                    return Card(

                                                                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                                      elevation: 16,
                                                                                      child: Column(
                                                                                          children: <Widget>[

                                                                                            Container(
                                                                                                height: MediaQuery.of(context).size.height * 0.08,
                                                                                                width: MediaQuery.of(context).size.width,
                                                                                                child: ColoredBox(
                                                                                                  color: mainColor,
                                                                                                  child: Stack(

                                                                                                      children:<Widget>[
                                                                                                        Align(
                                                                                                          alignment: Alignment.centerLeft,
                                                                                                          child: InkWell(
                                                                                                            onTap: () {
                                                                                                              Navigator.pop(context);
                                                                                                            },
                                                                                                            child: Icon(Icons.arrow_back, color: Colors.white),
                                                                                                          ),
                                                                                                        ),

                                                                                                        Align(
                                                                                                          alignment: Alignment.center,
                                                                                                          child: Text("Contact List", textAlign: TextAlign.center, style: TextStyle(
                                                                                                            fontWeight: FontWeight.bold,
                                                                                                            fontSize: 18,
                                                                                                            color: Colors.white,
                                                                                                          ),),
                                                                                                        ),

                                                                                                      ]),
                                                                                                )
                                                                                            ),
                                                                                            Container(height: 20, color: tabcolor),
                                                                                            DefaultTabController(
                                                                                              length: 3,
                                                                                              child: Column(
                                                                                                mainAxisSize: MainAxisSize.min,
                                                                                                children: <Widget>[
                                                                                                Container(
                                                                                                color: listcolor,

                                                                                                child: Material(
                                                                                                  color: listcolor,
                                                                                                  child: TabBar(
                                                                                                    indicatorColor: Colors.red,
                                                                                                    labelColor: Colors.red,
                                                                                                    unselectedLabelColor: mainColor,
                                                                                                    indicator: BoxDecoration(
                                                                                                        borderRadius: BorderRadius.circular(50),
                                                                                                        color: Colors.white),
                                                                                                    tabs: [
                                                                                                      Tab(
                                                                                                        child: Container(

                                                                                                          decoration: BoxDecoration(
                                                                                                            borderRadius: BorderRadius.circular(50),
                                                                                                            color: Colors.green,
                                                                                                          ),
                                                                                                          child: Align(
                                                                                                            alignment: Alignment.center,
                                                                                                            child:  Text(
                                                                                                              shafe,
                                                                                                              textAlign: TextAlign.center,
                                                                                                              style: TextStyle(
                                                                                                                fontSize: MediaQuery
                                                                                                                    .of(context)
                                                                                                                    .size
                                                                                                                    .height / 45,
                                                                                                                fontWeight: FontWeight.bold,
                                                                                                                color: Colors.white,
                                                                                                              ),

                                                                                                            ),
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                      Tab(
                                                                                                        child: Container(
                                                                                                          decoration: BoxDecoration(
                                                                                                              borderRadius: BorderRadius.circular(50),
                                                                                                              color:nyekundu,
                                                                                                              border: Border.all(color: Colors.redAccent, width: 1)),
                                                                                                          child: Align(
                                                                                                            alignment: Alignment.center,
                                                                                                            child:  Text(
                                                                                                              unshafe,
                                                                                                              textAlign: TextAlign.center,
                                                                                                              style: TextStyle(
                                                                                                                fontSize: MediaQuery
                                                                                                                    .of(context)
                                                                                                                    .size
                                                                                                                    .height / 45,
                                                                                                                fontWeight: FontWeight.bold,
                                                                                                                color: Colors.white,
                                                                                                              ),

                                                                                                            ),
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                      Tab(
                                                                                                        child: Container(
                                                                                                          decoration: BoxDecoration(
                                                                                                            borderRadius: BorderRadius.circular(50),
                                                                                                            color:mainColor,
                                                                                                          ),
                                                                                                          child: Align(
                                                                                                            alignment: Alignment.center,
                                                                                                            child:  Text(
                                                                                                              pendin,
                                                                                                              textAlign: TextAlign.center,
                                                                                                              style: TextStyle(
                                                                                                                fontSize: MediaQuery
                                                                                                                    .of(context)
                                                                                                                    .size
                                                                                                                    .height / 45,
                                                                                                                fontWeight: FontWeight.bold,
                                                                                                                color: Colors.white,
                                                                                                              ),

                                                                                                            ),
                                                                                                          ),
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),

                                                                                                ),
                                                                                              ),

                                                                                              SingleChildScrollView(
                                                                                                child: Container(
                                                                                                  height: MediaQuery.of(context).size.height*0.60,

                                                                                                  color: Colors.white,
                                                                                                  child: TabBarView(children: [
                                                                                                  Container(
                                                                                                  height: MediaQuery.of(context).size.height,


                                                                                                  child:SingleChildScrollView(
                                                                                                    physics: ScrollPhysics(),
                                                                                                    child: Column(
                                                                                                      mainAxisSize: MainAxisSize.min,
                                                                                                      children: <Widget>[

                                                                                                        Container(height: 10, color:Colors.white),
                                                                                                        Container(
                                                                                                          margin: const EdgeInsets.only(left: 6.0, right: 6.0, bottom: 5.0),

                                                                                                          height: 30,
                                                                                                          color: tabcolor,
                                                                                                          child: Row(
                                                                                                            children: <Widget>[
                                                                                                              Expanded(
                                                                                                                  flex: 5,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Name",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                              Expanded(
                                                                                                                  flex: 2,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Status",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                              Expanded(
                                                                                                                  flex: 3,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Contact",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                        Flexible(
                                                                                                          child: FutureBuilder(
                                                                                                            future: _getList(),
                                                                                                            builder: (BuildContext context,
                                                                                                                AsyncSnapshot snapshot) {


                                                                                                              if (snapshot.connectionState != ConnectionState.done ||
                                                                                                                  snapshot.hasData == null) {

                                                                                                                return CircularProgressIndicator();

                                                                                                              } else {
                                                                                                                print( "sasa ");

                                                                                                                if(snapshot.data.length == 0){
                                                                                                                  return Container(
                                                                                                                    height:300,
                                                                                                                      child: Center(
                                                                                                                        child: Text(
                                                                                                                            "No Contacts Found!",
                                                                                                                            style: TextStyle(    fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                                fontWeight: FontWeight.bold,
                                                                                                                                color: mainColor)
                                                                                                                        ),
                                                                                                                      )
                                                                                                                  );
                                                                                                                }
                                                                                                                return ListView.builder(
                                                                                                                  physics: NeverScrollableScrollPhysics(),
                                                                                                                  shrinkWrap: true,
                                                                                                                  itemCount: snapshot.data.length,
                                                                                                                  itemBuilder: (BuildContext context, int index) {
                                                                                                                    return
                                                                                                                      Container(
                                                                                                                        height: 80,
                                                                                                                        padding: const EdgeInsets.only(left: 4.0, right: 4.0, bottom: 5.0),
                                                                                                                        child:Card(
                                                                                                                          color: tabcolor,
                                                                                                                          elevation: 5,
                                                                                                                          child: Row(
                                                                                                                            children: <Widget>[
                                                                                                                              Expanded(
                                                                                                                                  flex: 5,
                                                                                                                                  child:  Container(
                                                                                                                                    child: Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: <Widget>[

                                                                                                                                        Center(child: Text(
                                                                                                                                          snapshot.data[index].name,

                                                                                                                                          style: TextStyle(
                                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                                            fontSize: MediaQuery
                                                                                                                                                .of(context)
                                                                                                                                                .size
                                                                                                                                                .height / 50,
                                                                                                                                          ),

                                                                                                                                        ),  ),

                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                  )
                                                                                                                              ),
                                                                                                                              Expanded(
                                                                                                                                  flex: 2,
                                                                                                                                  child:  Container(
                                                                                                                                    child: Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: <Widget>[

                                                                                                                                        Center(child: Container(

                                                                                                                                          width:40,
                                                                                                                                          child: RaisedButton(
                                                                                                                                            color: Colors.green,
                                                                                                                                            shape: RoundedRectangleBorder(
                                                                                                                                              borderRadius: BorderRadius.circular(15.0),
                                                                                                                                            ),
                                                                                                                                            onPressed: () {
                                                                                                                                            },
                                                                                                                                          ),
                                                                                                                                        ),
                                                                                                                                        ),

                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                  )
                                                                                                                              ),
                                                                                                                              Expanded(
                                                                                                                                flex: 3,
                                                                                                                                child: Row(
                                                                                                                                  children: <Widget>[
                                                                                                                                    Expanded(

                                                                                                                                        child:  Container(
                                                                                                                                          child: Column(
                                                                                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                            children: <Widget>[

                                                                                                                                              Center(child: IconButton(
                                                                                                                                                icon: Icon(Icons.call, color: Colors.green),
                                                                                                                                                onPressed: () {
                                                                                                                                                  _launchCaller(snapshot.data[index].telephone);
                                                                                                                                                },
                                                                                                                                              ),
                                                                                                                                              ),
                                                                                                                                            ],
                                                                                                                                          ),
                                                                                                                                        )
                                                                                                                                    ),
                                                                                                                                    Expanded(

                                                                                                                                        child:  Container(
                                                                                                                                          child: Column(
                                                                                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                            children: <Widget>[

                                                                                                                                              Center(child: IconButton(
                                                                                                                                                icon: Icon(Icons.message, color: Colors.blue),
                                                                                                                                                onPressed: () {
                                                                                                                                                  _textMe(snapshot.data[index].telephone);
                                                                                                                                                },
                                                                                                                                              ),
                                                                                                                                              ),

                                                                                                                                            ],
                                                                                                                                          ),
                                                                                                                                        )
                                                                                                                                    ),
                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      );
                                                                                                                  },
                                                                                                                );
                                                                                                              }
                                                                                                            },

                                                                                                          ),
                                                                                                        ),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                Container(
                                                                                                  height: MediaQuery.of(context).size.height,


                                                                                                  child:SingleChildScrollView(
                                                                                                    physics: ScrollPhysics(),
                                                                                                    child: Column(
                                                                                                      mainAxisSize: MainAxisSize.min,
                                                                                                      children: <Widget>[

                                                                                                        Container(height: 10, color:Colors.white),
                                                                                                        Container(
                                                                                                          margin: const EdgeInsets.only(left: 6.0, right: 6.0, bottom: 5.0),

                                                                                                          height: 30,
                                                                                                          color: tabcolor,
                                                                                                          child: Row(
                                                                                                            children: <Widget>[
                                                                                                              Expanded(
                                                                                                                  flex: 5,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Name",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                              Expanded(
                                                                                                                  flex: 2,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Status",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                              Expanded(
                                                                                                                  flex: 3,
                                                                                                                  child:  Container(
                                                                                                                    child: Column(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                      children: <Widget>[

                                                                                                                        Center(child: Text(
                                                                                                                          "Contact",
                                                                                                                          style: TextStyle(
                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                            fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                          ),

                                                                                                                        ),  ),

                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  )
                                                                                                              ),
                                                                                                            ],
                                                                                                          ),
                                                                                                        ),
                                                                                                        Flexible(
                                                                                                          child: FutureBuilder(
                                                                                                            future: _getListt(),
                                                                                                            builder: (BuildContext context,
                                                                                                                AsyncSnapshot snapshot) {


                                                                                                              if (snapshot.connectionState != ConnectionState.done ||
                                                                                                                  snapshot.hasData == null) {

                                                                                                                return CircularProgressIndicator();

                                                                                                              } else {
                                                                                                                print( "sasa ");

                                                                                                                if(snapshot.data.length == 0){
                                                                                                                  return Container(
                                                                                                                    height:300,
                                                                                                                      child: Center(
                                                                                                                        child: Text(
                                                                                                                            "No Contacts Found!",
                                                                                                                            style: TextStyle(    fontSize: MediaQuery
                                                                                                                                .of(context)
                                                                                                                                .size
                                                                                                                                .height / 40,
                                                                                                                                fontWeight: FontWeight.bold,
                                                                                                                                color: mainColor)
                                                                                                                        ),
                                                                                                                      )
                                                                                                                  );
                                                                                                                }
                                                                                                                return ListView.builder(
                                                                                                                  physics: NeverScrollableScrollPhysics(),
                                                                                                                  shrinkWrap: true,
                                                                                                                  itemCount: snapshot.data.length,
                                                                                                                  itemBuilder: (BuildContext context, int index) {
                                                                                                                    return
                                                                                                                      Container(
                                                                                                                        height: 80,
                                                                                                                        padding: const EdgeInsets.only(left: 4.0, right: 4.0, bottom: 5.0),
                                                                                                                        child:Card(
                                                                                                                          color: tabcolor,
                                                                                                                          elevation: 5,
                                                                                                                          child: Row(
                                                                                                                            children: <Widget>[
                                                                                                                              Expanded(
                                                                                                                                  flex: 5,
                                                                                                                                  child:  Container(
                                                                                                                                    child: Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: <Widget>[

                                                                                                                                        Center(child: Text(
                                                                                                                                          snapshot.data[index].name,

                                                                                                                                          style: TextStyle(
                                                                                                                                            color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                                            fontSize: MediaQuery
                                                                                                                                                .of(context)
                                                                                                                                                .size
                                                                                                                                                .height / 50,
                                                                                                                                          ),

                                                                                                                                        ),  ),

                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                  )
                                                                                                                              ),
                                                                                                                              Expanded(
                                                                                                                                  flex: 2,
                                                                                                                                  child:  Container(
                                                                                                                                    child: Column(
                                                                                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                      children: <Widget>[

                                                                                                                                        Center(child: Container(

                                                                                                                                          width:40,
                                                                                                                                          child: RaisedButton(
                                                                                                                                            color: Colors.red,
                                                                                                                                            shape: RoundedRectangleBorder(
                                                                                                                                              borderRadius: BorderRadius.circular(15.0),
                                                                                                                                            ),
                                                                                                                                            onPressed: () {
                                                                                                                                            },
                                                                                                                                          ),
                                                                                                                                        ),
                                                                                                                                        ),

                                                                                                                                      ],
                                                                                                                                    ),
                                                                                                                                  )
                                                                                                                              ),
                                                                                                                              Expanded(
                                                                                                                                flex: 3,
                                                                                                                                child: Row(
                                                                                                                                  children: <Widget>[
                                                                                                                                    Expanded(

                                                                                                                                        child:  Container(
                                                                                                                                          child: Column(
                                                                                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                            children: <Widget>[

                                                                                                                                              Center(child: IconButton(
                                                                                                                                                icon: Icon(Icons.call, color: Colors.green),
                                                                                                                                                onPressed: () {
                                                                                                                                                  _launchCaller(snapshot.data[index].telephone);
                                                                                                                                                },
                                                                                                                                              ),
                                                                                                                                              ),
                                                                                                                                            ],
                                                                                                                                          ),
                                                                                                                                        )
                                                                                                                                    ),
                                                                                                                                    Expanded(

                                                                                                                                        child:  Container(
                                                                                                                                          child: Column(
                                                                                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                            children: <Widget>[

                                                                                                                                              Center(child: IconButton(
                                                                                                                                                icon: Icon(Icons.message, color: Colors.blue),
                                                                                                                                                onPressed: () {
                                                                                                                                                  _textMe(snapshot.data[index].telephone);
                                                                                                                                                },
                                                                                                                                              ),
                                                                                                                                              ),

                                                                                                                                            ],
                                                                                                                                          ),
                                                                                                                                        )
                                                                                                                                    ),
                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      );
                                                                                                                  },
                                                                                                                );
                                                                                                              }
                                                                                                            },

                                                                                                          ),
                                                                                                        ),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                Container(
                                                                                                    height: MediaQuery.of(context).size.height,


                                                                                                    child:SingleChildScrollView(
                                                                                                    physics: ScrollPhysics(),
                                                                                                child: Column(
                                                                                                  mainAxisSize: MainAxisSize.min,
                                                                                                  children: <Widget>[

                                                                                                    Container(height: 10, color:Colors.white),
                                                                                                    Container(
                                                                                                      margin: const EdgeInsets.only(left: 6.0, right: 6.0, bottom: 5.0),

                                                                                                      height: 30,
                                                                                                      color: tabcolor,
                                                                                                      child: Row(
                                                                                                        children: <Widget>[
                                                                                                          Expanded(
                                                                                                              flex: 5,
                                                                                                              child:  Container(
                                                                                                                child: Column(
                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                  children: <Widget>[

                                                                                                                    Center(child: Text(
                                                                                                                      "Name",
                                                                                                                      style: TextStyle(
                                                                                                                        color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                        fontSize: MediaQuery
                                                                                                                            .of(context)
                                                                                                                            .size
                                                                                                                            .height / 40,
                                                                                                                      ),

                                                                                                                    ),  ),

                                                                                                                  ],
                                                                                                                ),
                                                                                                              )
                                                                                                          ),
                                                                                                          Expanded(
                                                                                                              flex: 2,
                                                                                                              child:  Container(
                                                                                                                child: Column(
                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                  children: <Widget>[

                                                                                                                    Center(child: Text(
                                                                                                                      "Status",
                                                                                                                      style: TextStyle(
                                                                                                                        color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                        fontSize: MediaQuery
                                                                                                                            .of(context)
                                                                                                                            .size
                                                                                                                            .height / 40,
                                                                                                                      ),

                                                                                                                    ),  ),

                                                                                                                  ],
                                                                                                                ),
                                                                                                              )
                                                                                                          ),
                                                                                                          Expanded(
                                                                                                              flex: 3,
                                                                                                              child:  Container(
                                                                                                                child: Column(
                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                  children: <Widget>[

                                                                                                                    Center(child: Text(
                                                                                                                      "Contact",
                                                                                                                      style: TextStyle(
                                                                                                                        color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                        fontSize: MediaQuery
                                                                                                                            .of(context)
                                                                                                                            .size
                                                                                                                            .height / 40,
                                                                                                                      ),

                                                                                                                    ),  ),

                                                                                                                  ],
                                                                                                                ),
                                                                                                              )
                                                                                                          ),
                                                                                                        ],
                                                                                                      ),
                                                                                                    ),
                                                                                                    Flexible(
                                                                                                      child: FutureBuilder(
                                                                                                        future: _getListtt(),
                                                                                                        builder: (BuildContext context,
                                                                                                            AsyncSnapshot snapshot) {


                                                                                                          if (snapshot.connectionState != ConnectionState.done ||
                                                                                                              snapshot.hasData == null) {

                                                                                                            return CircularProgressIndicator();

                                                                                                          } else {
                                                                                                            print( "sasa ");
                                                                                                            if(snapshot.data.length == 0){
                                                                                                              return Container(
                                                                                                                height:300,
                                                                                                                  child: Center(
                                                                                                                    child: Text(
                                                                                                                        "No Contacts Found!",
                                                                                                                        style: TextStyle(    fontSize: MediaQuery
                                                                                                                            .of(context)
                                                                                                                            .size
                                                                                                                            .height / 40,
                                                                                                                            fontWeight: FontWeight.bold,
                                                                                                                            color: mainColor)
                                                                                                                    ),
                                                                                                                  )
                                                                                                              );
                                                                                                            }

                                                                                                            return ListView.builder(
                                                                                                              physics: NeverScrollableScrollPhysics(),
                                                                                                              shrinkWrap: true,
                                                                                                              itemCount: snapshot.data.length,
                                                                                                              itemBuilder: (BuildContext context, int index) {
                                                                                                                return
                                                                                                                  Container(
                                                                                                                    height: 80,
                                                                                                                    padding: const EdgeInsets.only(left: 4.0, right: 4.0, bottom: 5.0),
                                                                                                                    child:Card(
                                                                                                                      color: tabcolor,
                                                                                                                      elevation: 5,
                                                                                                                      child: Row(
                                                                                                                        children: <Widget>[
                                                                                                                          Expanded(
                                                                                                                              flex: 5,
                                                                                                                              child:  Container(
                                                                                                                                child: Column(
                                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                  children: <Widget>[

                                                                                                                                    Center(child: Text(
                                                                                                                                      snapshot.data[index].name,

                                                                                                                                      style: TextStyle(
                                                                                                                                        color: mainColor, fontWeight: FontWeight.bold,
                                                                                                                                        fontSize: MediaQuery
                                                                                                                                            .of(context)
                                                                                                                                            .size
                                                                                                                                            .height / 50,
                                                                                                                                      ),

                                                                                                                                    ),  ),

                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                              )
                                                                                                                          ),
                                                                                                                          Expanded(
                                                                                                                              flex: 2,
                                                                                                                              child:  Container(
                                                                                                                                child: Column(
                                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                  children: <Widget>[

                                                                                                                                    Center(child: Container(

                                                                                                                                      width:40,
                                                                                                                                      child: RaisedButton(
                                                                                                                                        color: mainColor,
                                                                                                                                        shape: RoundedRectangleBorder(
                                                                                                                                          borderRadius: BorderRadius.circular(15.0),
                                                                                                                                        ),
                                                                                                                                        onPressed: () {
                                                                                                                                        },
                                                                                                                                      ),
                                                                                                                                    ),
                                                                                                                                    ),

                                                                                                                                  ],
                                                                                                                                ),
                                                                                                                              )
                                                                                                                          ),
                                                                                                                          Expanded(
                                                                                                                            flex: 3,
                                                                                                                            child: Row(
                                                                                                                              children: <Widget>[
                                                                                                                                Expanded(

                                                                                                                                    child:  Container(
                                                                                                                                      child: Column(
                                                                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                        children: <Widget>[

                                                                                                                                          Center(child: IconButton(
                                                                                                                                            icon: Icon(Icons.call, color: Colors.green),
                                                                                                                                            onPressed: () {
                                                                                                                                              _launchCaller(snapshot.data[index].telephone);
                                                                                                                                            },
                                                                                                                                          ),
                                                                                                                                          ),
                                                                                                                                        ],
                                                                                                                                      ),
                                                                                                                                    )
                                                                                                                                ),
                                                                                                                                Expanded(

                                                                                                                                    child:  Container(
                                                                                                                                      child: Column(
                                                                                                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                                                        children: <Widget>[

                                                                                                                                          Center(child: IconButton(
                                                                                                                                            icon: Icon(Icons.message, color: Colors.blue),
                                                                                                                                            onPressed: () {
                                                                                                                                              _textMe(snapshot.data[index].telephone);
                                                                                                                                            },
                                                                                                                                          ),
                                                                                                                                          ),

                                                                                                                                        ],
                                                                                                                                      ),
                                                                                                                                    )
                                                                                                                                ),
                                                                                                                              ],
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  );
                                                                                                              },
                                                                                                            );
                                                                                                          }
                                                                                                        },

                                                                                                      ),
                                                                                                    ),
                                                                                                  ],
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                          ]),
                                                                                    ),
                                                                                    ),
                                                                                    ],

                                                                                    ),
                                                                                    ),


                                                                                    Column(

                                                                                    children: <Widget>[
                                                                                    Container(height: 50, color: Colors.transparent),


                                                                                    ],
                                                                                    )
                                                                                    ],
                                                                                    ),
                                                                                    );
                                                                                  },
                                                                                );
                                                                              },
                                                                              child: Text(
                                                                                "View Status",
                                                                                style: TextStyle(
                                                                                  color: Colors.white,
                                                                                  fontSize: MediaQuery.of(context).size.height / 40,
                                                                                ),
                                                                              ),
                                                                            )
                                                                        ),


                                                                      ],
                                                                    )
                                                                  ],
                                                                ),
                                                              );
                                                            },
                                                          );
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),

                                          )
                                      );
                                    },
                                  );
                                }
                              },
                            ),


                          ),
                          Container(

                            child: FutureBuilder(
                              future: _getUserss(),
                              builder: (BuildContext context, AsyncSnapshot snapshot){
                                print(snapshot.data);
                                if(snapshot.data == null){
                                  return Container(
                                      child: Center(
                                          child: CircularProgressIndicator()
                                      )
                                  );
                                } else {
                                  if(snapshot.data.length == 0){
                                    return Container(
                                        child: Center(
                                            child: Text(
                                              "No Inactive Alerts Found!",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .height / 40,
                                                  fontWeight: FontWeight.bold
                                              ),

                                            ),
                                        )
                                    );
                                  }
                                  return ListView.builder(
                                    padding: EdgeInsets.only(bottom:300, top:5),
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return Card(
                                        margin: EdgeInsets.all(1.6),
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                        elevation: 4.0,
                                        child: Container(
                                            decoration: BoxDecoration(color: Colors.white,
                                              borderRadius: BorderRadius.circular(40),
                                            ),
                                            child:  ListTile(
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                                              leading: Container(
                                                  width:42,

                                                  child: Image(image: AssetImage(snapshot.data[index].imgUrl))


                                              ),



                                              title: Container(
                                                padding: EdgeInsets.only(right: 10.0, left:10.0),
                                                decoration: new BoxDecoration(
                                                    border: new Border(
                                                        right: new BorderSide(width: 1.5,   color:listcolor),
                                                        left: new BorderSide(width: 1.5,   color:listcolor))),

                                                child: Text(
                                                  snapshot.data[index].alertName,
                                                  style: TextStyle(color: mainColor),
                                                ),

                                              ),
                                        // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
                                        subtitle:                 Container(
                                          padding: EdgeInsets.only(right: 10.0, top:8.0 ,left:10.0),
                                          decoration: new BoxDecoration(
                                              border: new Border(
                                                  right: new BorderSide(width: 1.5,   color:listcolor),
                                                  left: new BorderSide(width: 1.5,   color:listcolor))),                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 3,
                                                  child:  RichText(
                                                    text: TextSpan(
                                                      children: <TextSpan>[
                                                        TextSpan(text: 'Total  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                        TextSpan(text:  snapshot.data[index].total.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                      ],
                                                    ),
                                                  )
                                              ),
                                              Expanded(
                                                flex: 3,
                                                child: Container(
                                                    child:  RichText(
                                                      text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(text: 'Viewed  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                          TextSpan(text: snapshot.data[index].viewed.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                        ],
                                                      ),
                                                    )
                                                ),
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Container(
                                                    child:  RichText(
                                                      text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(text: 'Not Viewed  ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                                          TextSpan(text:  snapshot.data[index].notViewed.toString(), style: TextStyle(fontWeight: FontWeight.normal, color:mainColor)),
                                                        ],
                                                      ),
                                                    )
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        trailing:

                                        Icon(Icons.keyboard_arrow_right, color: mainColor, size: 30.0),
                                        onTap: () {
                                          showDialog(
                                            context: context,
                                            builder: (context) {
                                              return Card(

                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                elevation: 16,
                                                child: Column(
                                                  children: <Widget>[

                                                    Container(
                                                        height: MediaQuery.of(context).size.height * 0.08,
                                                        width: MediaQuery.of(context).size.width,
                                                        child: ColoredBox(
                                                          color: mainColor,
                                                          child: Stack(

                                                              children:<Widget>[
                                                                Align(
                                                                  alignment: Alignment.centerLeft,
                                                                  child: InkWell(
                                                                    onTap: () {
                                                                      Navigator.pop(context);
                                                                    },
                                                                    child: Icon(Icons.arrow_back, color: Colors.white),
                                                                  ),
                                                                ),

                                                                Align(
                                                                  alignment: Alignment.center,
                                                                  child: Text(snapshot.data[index].heading, textAlign: TextAlign.center, style: TextStyle(
                                                                    fontWeight: FontWeight.bold,
                                                                    fontSize: 20,
                                                                    color: Colors.white,
                                                                  ),),
                                                                ),

                                                              ]),
                                                        )
                                                    ),
                                                    Container(height: 4, color: Colors.transparent),

                                                    Stack(

                                                        children:<Widget>[
                                                          Align(
                                                            alignment: Alignment.centerLeft,
                                                            child: Padding(
                                                              padding: const EdgeInsets.all(8.0),
                                                              child: Text(snapshot.data[index].alertTime.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                fontSize: 18,
                                                                color: mainColor,
                                                              ),),
                                                            ),
                                                          ),
                                                          Align(
                                                            alignment: Alignment.center,
                                                            child: Padding(
                                                              padding: const EdgeInsets.all(8.0),
                                                              child: Text(snapshot.data[index].alertDate.toString(), textAlign: TextAlign.center, style: TextStyle(
                                                                fontSize: 18,
                                                                color: mainColor,
                                                              ),),),
                                                          ),
                                                          Align(
                                                            alignment: Alignment.centerRight,
                                                            child:Container
                                                              ( height: 30,
                                                              child: Padding(
                                                                padding: const EdgeInsets.only(right: 15, top: 3),
                                                                child: Image(image: AssetImage(snapshot.data[index].imgUrl)
                                                                ),),),
                                                          )
                                                        ]),
                                                    Divider(
                                                      thickness: 1,
                                                      color: Color(0xff818181),
                                                    ),
                                                    Container(
                                                      height: MediaQuery
                                                          .of(context)
                                                          .size
                                                          .height * 0.40,
                                                      width: MediaQuery
                                                          .of(context)
                                                          .size
                                                          .width * 0.999,

                                                      child: Card(
                                                        elevation: 5.0,
                                                        color: listcolor,
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(30.0),
                                                        ),
                                                        child :Padding(
                                                          padding: const EdgeInsets.all(13.0),
                                                          child: Text(
                                                            snapshot.data[index].alertName.toString(),
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: mainColor,
                                                              fontSize: MediaQuery
                                                                  .of(context)
                                                                  .size
                                                                  .height / 40,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Container(height: 15, color: Colors.transparent),

                                                    Column(

                                                      children: <Widget>[
                                                        Container(height: 50, color: Colors.transparent),


                                                      ],
                                                    )
                                                  ],
                                                ),
                                              );
                                            },
                                          );
                                        },
                                      )
                                        ),
                                      );
                                    },
                                  );
                                }
                              },
                            ),


                          ),

                        ]),
                      ),
                    ],
                  ),
                ),

              ],
            ),
          ],

        ),
      ),
      bottomNavigationBar: Container(
        height: 40.0,
        child: BottomAppBar(
            color: mainColor,
            child: Container(
              margin: const EdgeInsets.only(left: 50.0, right: 50.0, top: 5, bottom: 5),

            )
        ),
      ),

    );
  }
  Future<void> _alert() async {

    if(akishoni=="Acknowledge")

    pr.show();
    print("saseni");
    print(id);
    print(lon);
    print(lat);
    String apiUrl = "http://alerts.p-count.org/mobile/V2/SFPAcknowledgeAlert";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"AcknowledmentID": id, "action": "Acknowledge","longitude": lon,"latitude": lat});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataStringg = response.body.toString();
print("aroo"+jsonsDataStringg);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Reporting succesful!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.black,
          textColor: Colors.black,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


void firebaseCloudMessaging_Listeners() {
  if (Platform.isIOS) iOS_Permission();

  _firebaseMessaging.getToken().then((token){
    print("antho token ni "+token);
    etokeni=token;
    _sendToken();
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    //  onBackgroundMessage: myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
  );
}


void iOS_Permission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true)
  );
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings)
  {
    print("Settings registered: $settings");
  });
}

Future onSelectNotification(String payload) async {
  showDialog(
    builder: (BuildContext context) {
      return new AlertDialog(
        title: Text("PayLoad"),
        content: Text("Payload : $payload"),
      );
    },
  );
}
void showNotification(String title, String body) async {
  await _demoNotification(title, body);
}

Future<void> _demoNotification(String title, String body) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID', 'channel name', 'channel description',
      importance: Importance.Max,
      playSound: true,
      sound: RawResourceAndroidNotificationSound('alarm.mp3'),
      showProgress: true,
      priority: Priority.High,
      ticker: 'test ticker');

  var iOSChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSChannelSpecifics);
  await flutterLocalNotificationsPlugin
      .show(0, title, body, platformChannelSpecifics, payload: 'test');
}
Future<void> _sendToken() async {
  print("aye enchogu "+ etokeni);
  String apiUrl = "http://alerts.p-count.org/mobile/V2/updateDeviceToken";
  Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
  final json =  convert.jsonEncode({  "DeviceToken": etokeni});
  http.Response response = await http.post(apiUrl,headers: headers, body: json);
  var jsonResponse = convert.jsonDecode(response.body);
  if (response.statusCode == 200) {

    String jsonsDataString = response.body.toString();
    print("jibu ni");
    print(jsonsDataString);

    Map<String, dynamic> map = jsonDecode(jsonsDataString);



  }
  else {
    print("no");
  }
}



class ActiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String action;
  String alertId;
  String total;
  String viewed;
  String notViewed;
  String heading;
  String AcknowledgementID;
  String AcknowledgementStatus;
  ActiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.alertId, this.total, this.viewed, this.notViewed, this.heading, this.AcknowledgementID, this.AcknowledgementStatus);

}

class InactiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String alertId;
  String total;
  String viewed;
  String notViewed;
  String heading;

  InactiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.alertId, this.total, this.viewed, this.notViewed, this.heading);

}

class ContactList {


  String index;
  String contactId;
  String name;
  String telephone;
  String email;
  String status;
  String coror;

  ContactList(this.index, this.contactId, this.name, this.telephone, this.email, this.status, this.coror);

}
class ContactListt {


  String index;
  String contactId;
  String name;
  String telephone;
  String email;
  String status;
  String coror;

  ContactListt(this.index, this.contactId, this.name, this.telephone, this.email, this.status, this.coror);

}
class ContactListtt {


  String index;
  String contactId;
  String name;
  String telephone;
  String email;
  String status;
  String coror;

  ContactListtt(this.index, this.contactId, this.name, this.telephone, this.email, this.status, this.coror);

}
Future<void> initPlatformState() async {


  print("Before set log");
  //OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  print("Before set user privacy");
  //OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);
  OneSignal.shared.setExternalUserId(id);

  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  OneSignal.shared.setNotificationReceivedHandler((notification) {


  });

  OneSignal.shared
      .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print("Payload only is"+result.notification.payload.jsonRepresentation().toString());

  });
  OneSignal.shared.setExternalUserId(id);

  OneSignal.shared
      .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
    print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
  });
  OneSignal.shared.setExternalUserId(id);


  // NOTE: Replace with your own app ID from https://www.onesignal.com
  await OneSignal.shared
      .init("d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0", iOSSettings: settings);

  OneSignal.shared.consentGranted(true);
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.getPermissionSubscriptionState();
  bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

}
