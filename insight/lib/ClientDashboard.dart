import 'dart:async';

import 'dart:convert';
import 'dart:io';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';


import 'InkWellDrawer.dart';
import 'ProfilePage.dart';

const nyekundu = Color(0xffe32e16);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const mainColor = Color(0xff403c3c);
const mainColorr = Color(0xff2e2e2e);
const mainColorrr = Color(0xff232323);
const listcolor = Color(0xffD3D3D3);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

String cdst="Fetching Location... ";
String jina=" ";
String address="Loading address... ";
String id="";
String lat="-1.04394";
String lon="37.0952399";
String alt=" ";
String town=" ";
String akishoni=" ";
String street=" ";
String fon=" ";
String emair=" ";
String nem=" ";
String lastCheck=" ";
ProgressDialog pr, prr;
LatLng _center;
String messo=" ";
String refreshh="0";
String mesho="No updates found in the last four hours";
String  company;
String usertokeni = '';
String clogo="http://alerts.p-count.org/k.png";
String alogo="http://alerts.p-count.org/dirLogos/rock_logo.PNG";
GoogleMapController controller;
int hesabu=0;
String defaultMessage = "Enchogu";
Set<Marker> markers;
LatLng currentLocation =
LatLng(-1.286389, 36.817223);
Marker m;
FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
String etokeni=" ";
List<ActiveAlerts> activeAlerts;
List<InactiveAlerts> inactiveAlerts;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();

class ClientDashboard extends StatefulWidget {

  _ClientDashboard createState() => _ClientDashboard();

}


class  _ClientDashboard extends State< ClientDashboard> {



  @override
  void initState() {
    super.initState();
    _restore();
    _determinePosition();
    initPlatformState();

    firebaseCloudMessaging_Listeners();
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        showNotification(
            message['notification']['title'], message['notification']['body']);
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
    const oneSec = const Duration(seconds:60);
    new Timer.periodic(oneSec, (Timer t) => setState(() {
      refreshh;
    }));
  }


  _restore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('name');
    usertokeni = prefs.getString('token');
    nem= prefs.getString('SFPName');
    fon= prefs.getString('SFPTelephone');
    emair= prefs.getString('SFPEmailAddress');
    clogo= prefs.getString('CompanyLogo');
    alogo= prefs.getString('AppLogo');
    id= prefs.getString('user_id');

    print("Aye enchogu, id ni "+id);
    setState(() {
      setState(() {
        OneSignal.shared.setExternalUserId(id);
        jina;
        lastCheck;
        usertokeni;
        nem;
        emair;
        fon;
      });
    });
  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    StreamSubscription<Position> positionStream = Geolocator.getPositionStream(desiredAccuracy: LocationAccuracy.best, timeInterval: 30000).listen(

            (position) async {
          print(DateTime.now());
          final coordinates = new Coordinates(position.latitude, position.longitude);
          setState(()
          {
            position;
            print("Sasa Antho");

            cdst= "("+position.latitude.toString() +", "+position.longitude.toString()+")"+position.altitude.toStringAsFixed(4);
            print(cdst);
            lat=position.latitude.toString();
            lon=position.longitude.toString();

          });
          setState(() async {
            var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
            var first = addresses.first;
            address=first.locality+", "+first.countryName;
            town=first.locality;
            street=first.addressLine;
          });

        });

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  Future<void> _alert() async {

    if(akishoni=="Acknowledge")
    {
      lon=" ";
      lat=" ";
    }
    pr.show();
    print("sasa");
    String apiUrl = "http://alerts.p-count.org/mobile/V2/AcknowledgeAlert";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({"id": id, "action": akishoni,"longitude": lon,"latitude": lat});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);

    String jsonsDataString = response.body.toString();
    print("alaa"+jsonsDataString);
    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });

      String jsonsDataString = response.body.toString();

      print(jsonsDataString);
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      setState(() { refreshh; });
      Fluttertoast.showToast(
          msg: "Reporting succesful!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0
      );

    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      print("no");
    }
  }

  Future<List<ActiveAlerts>> _getUsers() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getActiveAlerts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<ActiveAlerts> alerts = [];

      if (map['Active_Alerts'] != null) {
        print("sawasawa");
        activeAlerts = new List<ActiveAlerts>();
        map['Active_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/danger.png";
          if (v["Category"]=="Minor")
            urll="assets/information.png";
          if (v["Category"]=="Info")
            urll="assets/information.png";

          String s= v["PossibleAction"].toString();
          String ss;

          if(s=="[Safe, Unsafe]")
            ss="safe";
          if(s=="[Acknowledge]")
            ss="akno";
          if(s=="[]")
            ss="nothing";



          print(s);

          ActiveAlerts alert = ActiveAlerts(v["id"],v["cartegory"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll, ss,v["alert_title"],v["AlertState"].toString() );

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }
      String active=map['Active_Alerts'];
      print("nimepoteza mia mbili kwa njia "+active);
      print(map['Active_Alerts'].length);



    }
    else {
      print("no");
    }

  }
  Future<List<InactiveAlerts>> _getUserss() async {

    String apiUrl = "http://alerts.p-count.org/mobile/V2/getActiveAlerts";
    Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
    final json =  convert.jsonEncode({  "DeviceToken": etokeni});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    if (response.statusCode == 200) {

      String jsonsDataString = response.body.toString();

      Map<String, dynamic> map = jsonDecode(jsonsDataString);

      List<InactiveAlerts> alerts = [];

      if (map['Inactive_Alerts'] != null) {
        print("sawasawa");
        inactiveAlerts = new List<InactiveAlerts>();
        map['Inactive_Alerts'].forEach((v) {
          String urll;
          if (v["Category"]=="Major")
            urll="assets/danger.png";
          if (v["Category"]=="Minor")
            urll="assets/information.png";
          if (v["Category"]=="Info")
            urll="assets/information.png";



          InactiveAlerts alert = InactiveAlerts(v["id"],v["cartegory"],v["alert_name"], v["alertDate"], v["AlertTime"], v["AlertStatus"], urll,v["alert_title"] );

          alerts.add(alert);
          print("200 bob imepotea");
        });
        print(alerts.length);

        return alerts;

      }
      String active=map['Active_Alerts'];
      print("nimepoteza mia mbili kwa njia "+active);
      print(map['Active_Alerts'].length);



    }
    else {
      print("no");
    }

  }

  ListView _jobsListView(activeAlerts) {
    return ListView.builder(
      padding: EdgeInsets.only(bottom:120),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      itemCount: hesabu,
      itemBuilder: (BuildContext context, int index) {
        return userList(context, index);
      },
    );
  }
  Widget userList(BuildContext context, int index) {

  }
  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Reporting...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('hh:mm a').format(now);
    setState(() {
      formattedDate;
    });
    void handleClick(String value) {
      switch (value) {
        case 'Profile':
          Navigator.push(context,
              new MaterialPageRoute(builder: (ctxt) => new ProfilePage())
          );
          break;
      }
    }
    return Scaffold(
      backgroundColor: mainColorr,
      appBar: AppBar(
        backgroundColor: mainColorr,
        title: Image.network(clogo, fit: BoxFit.contain, height: 47
        ),
        centerTitle: true,

      ),
        body: SingleChildScrollView(
          child: Stack(

            children: <Widget>[

              Column(
                crossAxisAlignment: CrossAxisAlignment.center,

                children: <Widget>[

                  Container(height: 20, color: mainColorr),

                  Container(
                    color: mainColorr,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child:  Text(
                            "Last Seen : ",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color:nyeupe,

                            ),

                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            child:  Text(
                              formattedDate,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color:nyeupe,

                              ),

                            ),
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Container(
                            child:  Text(
                              address,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color:nyeupe,

                              ),

                            ),
                          ),
                        ),
                        Container(height: 10, color: mainColorr),
                      ],
                    ),
                  ),
                  Container(height: 10, color: mainColorr),

                  DefaultTabController(
                    length: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: 50.0,
                          decoration: BoxDecoration( 
                            color: mainColor,
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: TabBar(
                            indicator: BubbleTabIndicator(
                              tabBarIndicatorSize: TabBarIndicatorSize.tab,
                              indicatorHeight: 40.0,
                              indicatorColor: nyekundu,
                            ),

                            labelColor: nyeupe,
                            unselectedLabelColor: Colors.white,
                            tabs: [
                              Text('Active'),
                              Text('Inactive'),
                            ],
                            onTap: (index) {},
                          ),
                        ),
                        Container(
                          //Add this to give height
                          height: MediaQuery.of(context).size.height,
                          color: mainColor,
                          child: TabBarView(children: [
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUsers(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color: mainColor,
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  } else {
                                    if(snapshot.data.length == 0){
                                      return Container(
                                          color: mainColor,

                                          child: Center(
                                            child: Text(
                                                "No Active Alerts Found!",
                                                style: TextStyle(color: nyeupe)
                                            ),
                                          )
                                      );
                                    }
                                    return ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Card(
                                            color: mainColorr,

                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
                                          elevation: 2.0,
                                          child: Container(
                                              decoration: BoxDecoration(color: mainColorr,
                                                borderRadius: BorderRadius.circular(0),
                                              ),
                                        child:                   Container(
                                          padding: EdgeInsets.only(bottom:5, top:5, right: 6, left:6),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 2,
                                                child:  Container(

                                                    padding: EdgeInsets.only(bottom:35, top:35, right: 20, left:20),
                                                    child: Image(image: AssetImage(snapshot.data[index].imgUrl),  width: 20)


                                                ),
                                              ),

                                              Expanded(
                                                flex: 7,
                                                child: Container(
                                                  padding: EdgeInsets.only( top:3, right: 10, left:10),

                                                  decoration: new BoxDecoration(
                                                      border: new Border(
                                                          left: new BorderSide(width: 0.5,   color:listcolor))),
                                                  child:   Column(
                                                      children: <Widget>[
                                                        Container(
                                                          width: MediaQuery.of(context).size.width*0.7,
                                                          padding: EdgeInsets.all(12),
                                                          decoration: BoxDecoration(
                                                              color:listcolor,
                                                              border: Border.all(
                                                                color:listcolor,
                                                              ),
                                                              borderRadius: BorderRadius.all(Radius.circular(10))
                                                          ),
                                                          child: Text(
                                                            snapshot.data[index].alertName,
                                                            style: TextStyle(color: mainColorr),
                                                          ),

                                                        ),


                                                        (snapshot.data[index].action=="nothing"||snapshot.data[index].shtate=="1")
                                                            ?   Container(height: 3, color: Colors.transparent)

                                                            : Container(
                                                            padding: EdgeInsets.all(0), //<- try add this
                                                            margin: EdgeInsets.only(top: 10.0),
                                                            height: 60,

                                                            child: (snapshot.data[index].action=="safe")
                                                                ? Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: <Widget>[
                                                                Container(width: 50, height: 50, color: Colors.transparent),

                                                                Expanded(
                                                                  child: IconButton(
                                                                      icon: Image.asset('assets/shield.png'),
                                                                      iconSize: 100,

                                                                    onPressed: () {
                                                                      id= snapshot.data[index].index.toString();
                                                                      akishoni="Safe";
                                                                      _alert();
                                                                      refreshh;
                                                                    },
                                                                    ),


                                                                ),
                                                                Container(width: 20, color: Colors.transparent),
                                                                Expanded(

                                                                  child: IconButton(
                                                                    icon: Image.asset('assets/sikopoa.png'),
                                                                    iconSize: 100,

                                                                  onPressed: () {
                                                                id= snapshot.data[index].index.toString();
                                                                akishoni="UnSafe";
                                                                _alert();
                                                                },
                                                                  ),

                                                                ),

                                                              ],
                                                            )
                                                                : Container(
                                                              margin: const EdgeInsets.only(left: 65.0, right: 65.0),

                                                              child:  IconButton(
                                                                  icon: Icon(Icons.check, color: Colors.green, size: 40),
                                                                onPressed: ()  {
                                                            id= snapshot.data[index].index.toString();
                                                            akishoni="Acknowledge";
                                                            _alert();
                                                            },
                                                              ),

                                                            )
                                                        ),

                                                      ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),

                                              )
                                        );
                                      },
                                    );
                                  }
                                },
                              ),


                            ),
                            Container(
                              color: mainColor,
                              child: FutureBuilder(
                                future: _getUserss(),
                                builder: (BuildContext context, AsyncSnapshot snapshot){
                                  print(snapshot.data);
                                  if(snapshot.data == null){
                                    return Container(
                                        color: mainColor,
                                        child: Center(
                                            child: CircularProgressIndicator()
                                        )
                                    );
                                  } else {
                                    if(snapshot.data.length == 0){
                                      return Container(
                                          color: mainColor,

                                          child: Center(
                                            child: Text(
                                                "No Active Alerts Found!",
                                                style: TextStyle(color: nyeupe)
                                            ),
                                          )
                                      );
                                    }
                                    return ListView.builder(
                                      padding: EdgeInsets.only(bottom:300, top:5),
                                      itemCount: snapshot.data.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Card(
                                            color: mainColorr,

                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
                                            elevation: 2.0,
                                            child: Container(
                                              decoration: BoxDecoration(color: mainColorr,
                                                borderRadius: BorderRadius.circular(0),
                                              ),
                                              child:                   Container(
                                                padding: EdgeInsets.only(bottom:5, top:5, right: 6, left:6),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 2,
                                                      child:  Container(

                                                          padding: EdgeInsets.only(bottom:35, top:35, right: 20, left:20),
                                                          child: Image(image: AssetImage(snapshot.data[index].imgUrl),  width: 20)


                                                      ),
                                                    ),

                                                    Expanded(
                                                      flex: 7,
                                                      child: Container(
                                                        padding: EdgeInsets.only( top:3, right: 10, left:10),

                                                        decoration: new BoxDecoration(
                                                            border: new Border(
                                                                left: new BorderSide(width: 0.5,   color:listcolor))),
                                                        child:   Column(
                                                          children: <Widget>[
                                                            Container(
                                                              width: MediaQuery.of(context).size.width*0.7,
                                                              padding: EdgeInsets.all(12),
                                                              decoration: BoxDecoration(
                                                                  color:listcolor,
                                                                  border: Border.all(
                                                                    color:listcolor,
                                                                  ),
                                                                  borderRadius: BorderRadius.all(Radius.circular(10))
                                                              ),
                                                              child: Text(
                                                                snapshot.data[index].alertName,
                                                                style: TextStyle(color: mainColorr),
                                                              ),

                                                            ),


                                                              Container(height: 3, color: Colors.transparent)



                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            )
                                        );
                                      },
                                    );
                                  }
                                },
                              ),


                            ),

                          ]),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ],

          ),
        ),

    );
  }

  _textMe(String number) async {
    // Android
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      String uri = "sms:$number";
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _launchCaller(String number) async {
    String url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


void firebaseCloudMessaging_Listeners() {
  if (Platform.isIOS) iOS_Permission();

  _firebaseMessaging.getToken().then((token){
    print("antho token ni "+token);
    etokeni=token;
    _sendToken();
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    //  onBackgroundMessage: myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
  );
}


void iOS_Permission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true)
  );
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings)
  {
    print("Settings registered: $settings");
  });
}

Future onSelectNotification(String payload) async {
  showDialog(
    builder: (BuildContext context) {
      return new AlertDialog(
        title: Text("PayLoad"),
        content: Text("Payload : $payload"),
      );
    },
  );
}
void showNotification(String title, String body) async {
  await _demoNotification(title, body);
}

Future<void> _demoNotification(String title, String body) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channel_ID', 'channel name', 'channel description',
      importance: Importance.Max,
      playSound: true,
      sound: RawResourceAndroidNotificationSound('alarm.mp3'),
      showProgress: true,
      priority: Priority.High,
      ticker: 'test ticker');

  var iOSChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSChannelSpecifics);
  await flutterLocalNotificationsPlugin
      .show(0, title, body, platformChannelSpecifics, payload: 'test');
}
Future<void> _sendToken() async {
  print("aye enchogu "+ etokeni);
  String apiUrl = "http://alerts.p-count.org/mobile/V2/updateDeviceToken";
  Map<String, String> headers = {"Content-type": "application/json","token": usertokeni };
  final json =  convert.jsonEncode({  "DeviceToken": etokeni});
  http.Response response = await http.post(apiUrl,headers: headers, body: json);
  var jsonResponse = convert.jsonDecode(response.body);
  if (response.statusCode == 200) {

    String jsonsDataString = response.body.toString();
    print("jibu ni");
    print(jsonsDataString);

    Map<String, dynamic> map = jsonDecode(jsonsDataString);



  }
  else {
    print("no");
  }
}


class ActiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String action;
  String heading;
  String shtate;


  ActiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.action, this. heading, this. shtate);

}
class InactiveAlerts {
  int index;
  String category;
  String alertName;
  String alertDate;
  String alertTime;
  String alertStatus;
  String imgUrl;
  String heading;

  InactiveAlerts(this.index, this.category, this.alertName, this.alertDate, this.alertTime, this.alertStatus, this.imgUrl, this.heading);
}
Future<void> initPlatformState() async {


  print("Before set log");
  //OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  print("Before set user privacy");
  //OneSignal.shared.setRequiresUserPrivacyConsent(_requireConsent);
  OneSignal.shared.setExternalUserId(id);

  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  OneSignal.shared.setNotificationReceivedHandler((notification) {


  });

  OneSignal.shared
      .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print("Payload only is"+result.notification.payload.jsonRepresentation().toString());

  });
  OneSignal.shared.setExternalUserId(id);

  OneSignal.shared
      .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
    print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
  });
  OneSignal.shared.setExternalUserId(id);


  // NOTE: Replace with your own app ID from https://www.onesignal.com
  await OneSignal.shared
      .init("d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0", iOSSettings: settings);

  OneSignal.shared.consentGranted(true);
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);
  await OneSignal.shared.getPermissionSubscriptionState();
  bool requiresConsent = await OneSignal.shared.requiresUserPrivacyConsent();

}
class MyState extends StatefulWidget {
  @override
  _MyState createState() => _MyState();
}
class _MyState extends State<MyState> {
  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset('assets/$assetName', width: width);
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        home: Scaffold(
          backgroundColor: mainColorr,
          appBar: AppBar(      elevation: 0.0,
            bottomOpacity: 0.0,title: SafeArea(child:_buildImage('insightg.jpg', 100),),  toolbarHeight: 130, centerTitle: true, backgroundColor: mainColorr,),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: new Icon(Icons.person_outline_outlined, color: nyeupe,),
                          highlightColor: Colors.pink,
                          onPressed: (){
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (_) => ProfilePage()),
                            );
                          },
                        ),

                        Text("Profile", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: new Icon(Icons.notifications_none_outlined, color: nyeupe,),
                          highlightColor: Colors.pink,
                          onPressed: (){
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (_) => ClientDashboard()),
                            );
                          },
                        ),
                        Text("Alerts", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: nyekundu,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: new Icon(Icons.add_call, color: nyeupe,),
                          highlightColor: Colors.pink,
                          onPressed: (){
                            showDialog(
                              context: context,
                              builder: (context) {
                                return Card(

                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  elevation: 16,
                                  child: Column(
                                    children: <Widget>[

                                      Container(
                                          height: MediaQuery.of(context).size.height * 0.08,
                                          width: MediaQuery.of(context).size.width,
                                          child: ColoredBox(
                                            color: mainColor,
                                            child: Stack(

                                                children:<Widget>[
                                                  Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: InkWell(
                                                      onTap: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Icon(Icons.arrow_back, color: Colors.white),
                                                    ),
                                                  ),

                                                  Align(
                                                    alignment: Alignment.center,
                                                    child: Text("Emergency Contacts", textAlign: TextAlign.center, style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 20,
                                                      color: Colors.white,
                                                    ),),
                                                  ),

                                                ]),
                                          )
                                      ),

                                      Column(

                                        children: <Widget>[


                                          Container(
                                            margin: const EdgeInsets.only(right: 8.0, left: 8.0),
                                            decoration: BoxDecoration(color: mainColor),
                                            width: MediaQuery.of(context).size.width,
                                            height: MediaQuery.of(context).size.height * 0.40,
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(height: 20),
                                                SizedBox(height: 20),
                                                Icon(
                                                  Icons.person,
                                                  color: Colors.white,
                                                  size: 160,
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.all(8),
                                                      child: Text(
                                                        nem,
                                                        style: TextStyle(color: Colors.white, fontSize: 30),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              children: <Widget>[
                                                Card(
                                                  child: ListTile(
                                                    title:Text(
                                                      fon,
                                                      style: TextStyle(color: Colors.black54),
                                                    ),
                                                    subtitle: Text(
                                                      "SFP Telephone Number",
                                                      style: TextStyle(color: Colors.black54),
                                                    ),
                                                    leading: IconButton(
                                                      icon: Icon(Icons.phone, color:mainColor),
                                                      onPressed: () {
                                                        _launchCaller(fon);
                                                      },
                                                    ),
                                                    trailing: IconButton(
                                                      icon: Icon(Icons.message),
                                                      onPressed: () {
                                                        _textMe(fon);
                                                      },
                                                    ),
                                                  ),
                                                ),
                                                Card(
                                                  child: ListTile(
                                                    title: Text( emair ),
                                                    subtitle: Text(
                                                      "SFP Email address",
                                                      style: TextStyle(color: Colors.black54),
                                                    ),
                                                    leading: IconButton(
                                                        icon: Icon(Icons.email, color: mainColor),
                                                        onPressed: () {}),
                                                  ),
                                                ),


                                              ],
                                            ),
                                          )

                                        ],
                                      )
                                    ],
                                  ),
                                );
                              },
                            );

                          },
                        ),
                        Text("Emergency", style: TextStyle(color: nyeupe)),
                        Text("Contact", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: new  Icon(Icons.info_outline, color: nyeupe,),
                          highlightColor: Colors.pink,
                          onPressed: (){
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (_) => OnBoardingPage()),
                            );
                          },
                        ),
                        Text("About", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),

            ],
          ),
        ));
  }
}
class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => MyState()),
    );
  }

  Widget _buildFullscrenImage() {
    return Image.asset(
      'assets/insightg.jpg',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset('assets/$assetName', width: width);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 25.0, color: nyeupe);

    const pageDecoration = const PageDecoration(
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: mainColorr,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: mainColorr,
      globalFooter: Container(
        color: mainColor,
        width: double.infinity,
        height: 60,
        child: ElevatedButton(
          child: const Text(
            'Let\s go right away!',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),
          ),
          onPressed: () => _onIntroEnd(context),
          style: ElevatedButton.styleFrom(primary: mainColor),
        ),
      ),
      pages: [
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topCenter,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 2),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: "Welcome to ",
                        style: TextStyle(color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),
                    TextSpan(
                        text: "i",
                        style: TextStyle(color: Colors.red,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),
                    TextSpan(
                        text: "Secure\nThe app that keeps you safe.",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),

                  ]),
                ),

              ),
            ),
          ),
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),

                  child: Container(
                    height: 180,
                    width: 180,
                    child: Image(
                      image: AssetImage('assets/insightg.jpg'),
                      alignment: Alignment.center,
                      height: double.infinity,
                      width: double.infinity,
                      fit: BoxFit.fill,
                    ),

                  )
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: <TextSpan>[

                  TextSpan(
                      text: "\nThis app is for authorised users. Please Log in with your registered email and password.",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),

                ]),
              ),

            ],
          ),
          decoration: pageDecoration.copyWith(
            bodyFlex: 5,
            bodyAlignment: Alignment.center,
          ),
        ),
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topLeft,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 16, left: 16),
                child: _buildImage('insightg.jpg', 100),
              ),
            ),
          ),
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                textAlign: TextAlign.start,
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                      text: "i",
                      style: TextStyle(color: Colors.red,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),
                  TextSpan(
                      text: "Secure is an informaton. alerting and accountability app. \n\nYou will receive pretinent security alerts. \n\nIn the event of a major incident, the app can be used to quickly account for personel \n\nIt is simple and easy to use.\n",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),

                ]),
              ),

            ],
          ),
          decoration: pageDecoration.copyWith(
            bodyFlex: 5,
            imageFlex: 1,
            bodyAlignment: Alignment.center,
            imageAlignment: Alignment.topCenter,
          ),
        ),
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topCenter,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 50, left: 16),
                child: Text("This is your Dashboard", style: TextStyle(fontSize: 24.0, color: nyeupe)),

              ),
            ),
          ),
          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.16, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.person_outline_outlined, color: nyeupe,),
                        Text("Profile", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.notifications_none_outlined, color: nyeupe,),
                        Text("Alerts", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: nyekundu,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.add_call, color: nyeupe,),
                        Text("Emergency", style: TextStyle(color: nyeupe)),
                        Text("Contact", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.info_outline, color: nyeupe,),
                        Text("About", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),

            ],
          ),


        ),
        PageViewModel(
          titleWidget:SafeArea(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(top: 50),
                  height: MediaQuery.of(context).size.width*0.3,
                  width: MediaQuery.of(context).size.width*0.3,
                  decoration: BoxDecoration(
                    color: mainColorrr,
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                        bottomRight: Radius.circular(20)),
                    border: Border.all(color: mainColor),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 2.0,
                        spreadRadius: 0.0,
                        offset: Offset(2.0, 2.0), // shadow direction: bottom right
                      )
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.person_outline_outlined, color: nyeupe,),
                      Text("Profile", style: TextStyle(color: nyeupe)),
                    ],
                  ),

                ) ,
                Container(width: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

                Flexible(
                    child: new Text("View and Update your Details in the profile section.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),

              ],
            ),
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

              Text("This is important when contacting you incase an emergency arises.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height*0.4,
                    width: MediaQuery.of(context).size.width*0.5,
                    child: Image.asset(
                      'assets/icoca.png',
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    ),

                  ) ,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width*0.3,
                        width: MediaQuery.of(context).size.width*0.3,
                        decoration: BoxDecoration(
                          color: mainColorrr,
                          borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                              bottomRight: Radius.circular(20)),
                          border: Border.all(color: mainColor),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(2.0, 2.0), // shadow direction: bottom right
                            )
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.notifications_none_outlined, color: nyeupe,),
                            Text("Alerts", style: TextStyle(color: nyeupe)),
                          ],
                        ),

                      ) ,
                      Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
                      Text("Access your", textAlign: TextAlign.left, style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
                      Text("alerts here.", textAlign: TextAlign.left, style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
                    ],
                  ),
                ],
              ),

            ],
          ),


        ),
        PageViewModel(
          titleWidget:SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[



                Flexible(
                    child: new Text("You will receive alerts on major security issues and pertinent security information.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                _buildImage('active.png', 100),            ],
            ),
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

              Text("You will respond to this alerts.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),

              Container(
                height: MediaQuery.of(context).size.width*0.25,
                width: MediaQuery.of(context).size.width*0.8,
                decoration: BoxDecoration(
                  color: mainColorrr,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height*0.05,
                      child: Image.asset(
                        'assets/danger.png',
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                      ),

                    ) ,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Safe       ",
                              ),
                              WidgetSpan(
                                child: _buildImage('shield.png', 35),
                              ),
                            ],
                          ),
                        ),
                        Container(height: 20, color: Colors.transparent),

                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Unsafe       ",
                              ),
                              WidgetSpan(
                                child: _buildImage('sikopoa.png', 35),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

              ) ,
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
              Container(
                height: MediaQuery.of(context).size.width*0.25,
                width: MediaQuery.of(context).size.width*0.8,
                decoration: BoxDecoration(
                  color: mainColorrr,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height*0.05,
                      child: Image.asset(
                        'assets/information.png',
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                      ),

                    ) ,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Acknowledge       ",
                              ),
                              WidgetSpan(
                                child: Icon(Icons.check, color: Colors.green, size: 35),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

              ) ,

              Container(height: MediaQuery.of(context).size.height*0.25, color: Colors.transparent),

            ],
          ),


        ),
        PageViewModel(
          titleWidget:SafeArea(
            child: Column(

              children: [
                Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                  children: <Widget>[



                    Flexible(
                        child: new Text("Whenever in trouble, use the emergency contact information.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                  ],
                ),
              ],
            ),
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Container(
                height: MediaQuery.of(context).size.width*0.3,
                width: MediaQuery.of(context).size.width*0.3,
                decoration: BoxDecoration(
                  color: nyekundu,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.add_call, color: nyeupe,),
                    Text("Emergency", style: TextStyle(color: nyeupe)),
                    Text("Contact", style: TextStyle(color: nyeupe)),
                  ],
                ),

              ) ,
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[



                  Flexible(
                      child: new Text("This will automatically call us directly and send your location to Insights Regional Operations Center.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                ],
              ),
              Align(
                alignment: Alignment.centerRight,
                child:  _buildImage('dunia.png', 180),
              ),


            ],
          ),


        ),
        PageViewModel(
          titleWidget:Align(
              alignment: Alignment.topCenter,
              child:  _buildImage('insightg.jpg',100)
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[



                  Flexible(
                      child: new Text("Insights 24 hours Regional Operation Center (ROC) is manned by security professionalss who are able to offer advice and coordinate assistance for our clients.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height*0.32,
                width:  MediaQuery.of(context).size.height*0.32,
                child: Image(
                  image: AssetImage('assets/akili.png'),
                  alignment: Alignment.center,
                  height: double.infinity,
                  width: double.infinity,
                  fit: BoxFit.fill,
                ),

              )



            ],
          ),

          decoration: pageDecoration.copyWith(
            bodyAlignment: Alignment.center,
            imageAlignment: Alignment.topCenter,
          ),
        ),

      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      //rtl: true, // Display as right-to-left
      skip: const Text('Skip',  style: TextStyle(color: Colors.red
      )),
      next: const Icon(Icons.arrow_forward, color: nyekundu,),
      done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.red)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: false
          ? const EdgeInsets.all(12.0)
          : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeColor:Colors.red,
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: mainColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}
_textMe(String number) async {
  // Android
  String uri = "sms:$number";
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    String uri = "sms:$number";
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}

_launchCaller(String number) async {
  String url = "tel:$number";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}