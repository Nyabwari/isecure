import 'dart:convert';
import 'dart:io';
import 'package:introduction_screen/introduction_screen.dart';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert' as convert;
import 'package:flutter_session/flutter_session.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';



import 'dart:convert';

import 'ClientDashboard.dart';
import 'dart:io';

import 'SFDashBoard.dart';

String os = Platform.operatingSystem;
String user, jina, etoken=" ";
ProgressDialog pr;
String role="0";


FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
const mainColor = Color(0xff403c3c);
const mainColorr = Color(0xff2e2e2e);
const mainColorrr = Color(0xff232323);
const nyekundu = Color(0xffe82224);
const tabcolor = Color(0xffe1dede);
const nyeupe = Color(0xffffffff);
const listcolor = Color(0xffD3D3D3);
const kijani = Colors.green;
const gray = Color(0xffa9a9a9);

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  jina = prefs.getString('user_id');
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.init(
      "d64f6095-b8ff-41bc-aaf9-d9d4e6f7e5d0",

      iOSSettings: {
        OSiOSSettings.autoPrompt: false,
        OSiOSSettings.inAppLaunchUrl: false
      }
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);

  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    jina = prefs.getString('user_id');
    role= prefs.getString('isSFP');
    print(jina);
    if(jina!=null)
      return true;
    else
      return false;
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 4)),
      builder: (context, AsyncSnapshot snapshot) {
        // Show splash screen while waiting for app resources to load:
        if (snapshot.connectionState == ConnectionState.waiting) {
          return MaterialApp(home: Splash());
        } else {
          // Loading is done, return the app:
          return MaterialApp(
              home: FutureBuilder(
                future: isLoggedIn(),
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    return snapshot.data ? ((role=="1")? MyStatee(): MyState()) : OnBoardingPage() ;
                  }
                  return Container(); // noop, this builder is called again when the future completes
                },
              )
          );
        }
      },
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  String email, password;
  String username;
  String userId;
  int companyId;
  String companyName;
  List<Null> emergencyContacts;
  String latestMessage;
  String iconUrl;
  bool isAuthenticated;
  String authMessage;
  final _text = TextEditingController();
  bool _validate = false;
  final _formKey = GlobalKey<FormState>();
  final _formKeyy = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();

  }


  Widget _buildLogo() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,

      children: <Widget>[
        new
        Expanded(
            child: Column(children: <Widget>[
            Container(
            width: 120.0,
            child: FittedBox(
            fit: BoxFit.contain,
            child:Image.asset(
              'assets/insightg.jpg',
            ),),),


              Container(
                width: 120.0,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "i",
                          style: TextStyle(color: Colors.red,
                            fontFamily: 'Microsoft Himalaya',
                          )),
                      TextSpan(
                          text: "Secure",
                          style: TextStyle(
                              color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                          )),

                    ]),
                  )

                ),
              ),


            ],)
        ),

      ],
    );
  }

  Future<void> _doSignIn() async {
    print(email);
    print(password);
    print(os);
    print(etoken);
    pr.show();
    String apiUrl = "http://alerts.p-count.org/mobile/V2/userlogin";
    Map<String, String> headers = {"Content-type": "application/json"};
    final json =  convert.jsonEncode({"email": email, "password": password,"DeviceType": os,"DeviceToken": etoken});
    http.Response response = await http.post(apiUrl,headers: headers, body: json);
    String jsonsDataString = response.body.toString();

    if (response.statusCode == 200) {
      pr.hide().then((isHidden) {
        print(isHidden);
      });



      Map<String, dynamic> json = jsonDecode(jsonsDataString);

      Map<String, dynamic> map=(json['basic_details']);
      Map<String, dynamic> mapp=(json['EmegencyDetails']);
      print("idd ni "+jsonsDataString);

      print("idd ni "+map.toString());
      if(map==null)
      {
        Fluttertoast.showToast(
            msg: "Incorrect Username Or Password",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }


      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("user_id",map['user_id'].toString());
      prefs.setString("token", map['token'].toString());
      prefs.setString("name",map['name'].toString());
      prefs.setString("telephone",map['telephone'].toString());
      prefs.setString("UserProfile",map['UserProfile'].toString());
      prefs.setString("email",map['email'].toString());
      prefs.setString("position",map['position'].toString());
      prefs.setString("Department",map['Department'].toString());
      prefs.setString("dob",map['dob'].toString());
      prefs.setString("home_location",map['home_location'].toString());
      prefs.setString("work_location",map['work_location'].toString());
      prefs.setString("emergency_contact",map['emergency_contact'].toString());
      prefs.setString("gender",map['gender'].toString());
      prefs.setString("position",map['position'].toString());
      prefs.setString("ip_id",map['ip_id'].toString());
      prefs.setString("isSFP",map['isSFP'].toString());
      prefs.setString("Role",map['Role'].toString());
      prefs.setString("LastSeenDate",map['LastSeenDate'].toString());
      prefs.setString("LastSeenLocation",map['LastSeenLocation'].toString());
      prefs.setString("CompanyName",map['CompanyName'].toString());
      prefs.setString("CompnayTelephone",map['CompnayTelephone'].toString());

      prefs.setString("SFPName",mapp['SFPName'].toString());
      prefs.setString("SFPTelephone",mapp['SFPTelephone'].toString());
      prefs.setString("SFPEmailAddress",mapp['SFPEmailAddress'].toString());

      prefs.setString("CompanyLogo",map['CompanyLogo'].toString());
      prefs.setString("AppLogo",map['AppLogo'].toString());

      print("print tok ni "+map['SFPEmailAddress'].toString());
      print(map['CompanyLogo'].toString());
      print(map['email'].toString());

    print(map['isSFP']);
      if(map['isSFP']==0)
        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => new MyState())
        );
      else
        Navigator.push(context, new MaterialPageRoute(
            builder: (context) => new MyStatee())
        );
    }
    else {
      pr.hide().then((isHidden) {
        print(isHidden);
      });
      Fluttertoast.showToast(
          msg: "Incorrect Username Or Password",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 3,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      print("nooo");
    }
  }


  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    pr.style(
        message: 'Logging in...',
        borderRadius: 10.0,
        backgroundColor: Colors.grey.withOpacity(0.8),
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    return Scaffold(
      backgroundColor: Color(0xff2e2e2e),

      body: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: IntrinsicHeight(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                // CONTENT HERE
                Spacer(),
                _buildLogo(),
                Container(height: 30, color: Colors.transparent),
                _buildContainer(),
                Container(height: 35, color: Colors.transparent),
              ],
            ),
          ),
        ),

      bottomNavigationBar:  Container(
          padding: EdgeInsets.only(bottom: 5, top: 5),
          color:listcolor,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                new RichText(
                  text: new TextSpan(text: 'By signing in, I agree to ', style: TextStyle(
                    color: Colors.black,
                  ), children: [
                    TextSpan(
                      text: 'Terms and Conditions',
                      style: TextStyle(
                        color: nyekundu,
                        decoration: TextDecoration.underline,
                      ),
                      recognizer: new TapGestureRecognizer()..onTap = () => -_launchTerms(),
                    ),

                  ]),
                ),
              ]
          )

      ),
    );
  }
  Widget _buildEmailRow() {
    return Padding(
      key: _formKey,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          onChanged: (value) {
            setState(() {
             email = value;
            });
          },
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.person,
            color: nyekundu,
          ),
          contentPadding: EdgeInsets.all(0),
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(15.0),


            ),

          ),
          labelText: "Email",
        ),
      ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
    ),
    );
  }

  Widget _buildPasswordRow() {
    return Padding(
      key: _formKeyy,
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Container(
        child: TextFormField(
          obscureText: true,
      onChanged: (value) {
          setState(() {
            password = value;
          });
      },
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.lock,
              color: nyekundu,
            ),
            contentPadding: EdgeInsets.all(0),
            border: new OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(15.0),
              ),
            ),
            labelText: "Password",
          ),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _buildForgetPasswordButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        FlatButton(
          onPressed: () {},
          child: Text("Forgot Password"),
        ),
      ],
    );
  }

  Widget _buildLoginButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 1.4 * (MediaQuery.of(context).size.height / 25),
          width: 6.5 * (MediaQuery.of(context).size.width / 10),
          margin: EdgeInsets.only(bottom: 20),
          child: RaisedButton(
            elevation: 5.0,
            color: nyekundu,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            onPressed: () {
              _doSignIn();
            },
            child: Text(
              "Log In",
              style: TextStyle(
                color: Colors.white,
                fontSize: MediaQuery.of(context).size.height / 40,
              ),
            ),
          ),
        )
      ],
    );
  }
  Widget _buildContainer() {
    _firebaseMessaging.getToken().then((token){
      print("token ni "+token);
    });

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.53,
            width: MediaQuery.of(context).size.width * 0.92,
            decoration: BoxDecoration(
              color: listcolor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[


                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                            text: "Log In",
                            style: TextStyle(
                              fontSize: MediaQuery.of(context).size.height / 20,
                              color: nyekundu,
                              fontFamily: 'Microsoft Himalaya',
                            )),

                      ]),
                    ),
                  ],
                ),
                Container(height: 30, color: Colors.transparent),

                _buildEmailRow(),
                Container(height: 30, color: Colors.transparent),
                _buildPasswordRow(),
                Container(height: 40, color: Colors.transparent),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Forgot Password",
                      style: TextStyle(
                        fontSize: MediaQuery.of(context).size.height / 40,
                      ),
                    ),
                  ],
                ),
                Container(height: 10, color: Colors.transparent),
                _buildLoginButton(),

              ],
            ),
          ),
        ),
      ],
    );
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token){
      etoken=token;
      print("token ni "+token);
      print("os ni "+os);

    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }
}


class Splash extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Widget _buildLogoo() {
      return  Container(

        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: FittedBox(
          fit: BoxFit.contain,
          child:Image.asset(
            'assets/enchogu.gif',
          ),),
      );

    }
    return Scaffold(
      backgroundColor: mainColorr,

      body: Center(
        child: _buildLogoo(),
      ),
    );
  }
}
_launchTerms() async {
  const url = 'http://p-count.org/termandconditions';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => LoginPage()),
    );
  }

  Widget _buildFullscrenImage() {
    return Image.asset(
      'assets/insightg.jpg',
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset('assets/$assetName', width: width);
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 25.0, color: nyeupe);

    const pageDecoration = const PageDecoration(
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: mainColorr,
    );

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: mainColorr,
      globalFooter: Container(
        color: mainColor,
        width: double.infinity,
        height: 60,
        child: ElevatedButton(
          child: const Text(
            'Let\s go right away!',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.red),
          ),
          onPressed: () => _onIntroEnd(context),
          style: ElevatedButton.styleFrom(primary: mainColor),
        ),
      ),
      pages: [
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topCenter,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 2),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                        text: "Welcome to ",
                        style: TextStyle(color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),
                    TextSpan(
                        text: "i",
                        style: TextStyle(color: Colors.red,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),
                    TextSpan(
                        text: "Secure\nThe app that keeps you safe.",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Microsoft Himalaya',
                            fontSize: 35.0
                        )),

                  ]),
                ),

              ),
            ),
          ),
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),

                child: Container(
                  height: 180,
                  width: 180,
                  child: Image(
                    image: AssetImage('assets/insightg.jpg'),
                    alignment: Alignment.center,
                    height: double.infinity,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),

                )
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: <TextSpan>[

                  TextSpan(
                      text: "\nThis app is for authorised users. Please Log in with your registered email and password.",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),

                ]),
              ),

            ],
          ),
          decoration: pageDecoration.copyWith(
            bodyFlex: 5,
            bodyAlignment: Alignment.center,
          ),
        ),
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topLeft,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 16, left: 16),
                child: _buildImage('insightg.jpg', 100),
              ),
            ),
          ),
          bodyWidget: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                textAlign: TextAlign.start,
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                      text: "i",
                      style: TextStyle(color: Colors.red,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),
                  TextSpan(
                      text: "Secure is an informaton. alerting and accountability app. \n\nYou will receive pretinent security alerts. \n\nIn the event of a major incident, the app can be used to quickly account for personel \n\nIt is simple and easy to use.\n",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Microsoft Himalaya',
                          fontSize: 35.0
                      )),

                ]),
              ),

            ],
          ),
          decoration: pageDecoration.copyWith(
            bodyFlex: 5,
            imageFlex: 1,
            bodyAlignment: Alignment.center,
            imageAlignment: Alignment.topCenter,
          ),
        ),
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topCenter,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 50, left: 16),
                child: Text("This is your Dashboard", style: TextStyle(fontSize: 24.0, color: nyeupe)),

              ),
            ),
          ),
          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.16, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.person_outline_outlined, color: nyeupe,),
                        Text("Profile", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.notifications_none_outlined, color: nyeupe,),
                        Text("Alerts", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: nyekundu,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.add_call, color: nyeupe,),
                        Text("Emergency", style: TextStyle(color: nyeupe)),
                        Text("Contact", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,
                  Container(
                    height: MediaQuery.of(context).size.width*0.3,
                    width: MediaQuery.of(context).size.width*0.3,
                    decoration: BoxDecoration(
                      color: mainColorrr,
                      borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                          bottomRight: Radius.circular(20)),
                      border: Border.all(color: mainColor),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.info_outline, color: nyeupe,),
                        Text("About", style: TextStyle(color: nyeupe)),
                      ],
                    ),

                  ) ,        ],
              ),

            ],
          ),


        ),
        PageViewModel(
          titleWidget:SafeArea(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(top: 50),
                  height: MediaQuery.of(context).size.width*0.3,
                  width: MediaQuery.of(context).size.width*0.3,
                  decoration: BoxDecoration(
                    color: mainColorrr,
                    borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                        bottomRight: Radius.circular(20)),
                    border: Border.all(color: mainColor),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 2.0,
                        spreadRadius: 0.0,
                        offset: Offset(2.0, 2.0), // shadow direction: bottom right
                      )
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.person_outline_outlined, color: nyeupe,),
                      Text("Profile", style: TextStyle(color: nyeupe)),
                    ],
                  ),

                ) ,
                Container(width: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

                Flexible(
                    child: new Text("View and Update your Details in the profile section.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),

              ],
            ),
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

              Text("This is important when contacting you incase an emergency arises.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height*0.4,
                    width: MediaQuery.of(context).size.width*0.5,
                    child: Image.asset(
                      'assets/icoca.png',
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    ),

                  ) ,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.width*0.3,
                        width: MediaQuery.of(context).size.width*0.3,
                        decoration: BoxDecoration(
                          color: mainColorrr,
                          borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                              bottomRight: Radius.circular(20)),
                          border: Border.all(color: mainColor),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(2.0, 2.0), // shadow direction: bottom right
                            )
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.notifications_none_outlined, color: nyeupe,),
                            Text("Alerts", style: TextStyle(color: nyeupe)),
                          ],
                        ),

                      ) ,
                      Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
                      Text("Access your", textAlign: TextAlign.left, style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
                      Text("alerts here.", textAlign: TextAlign.left, style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
                    ],
                  ),
                ],
              ),

            ],
          ),


        ),
        PageViewModel(
          titleWidget:SafeArea(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[



                Flexible(
                    child: new Text("You will receive alerts on major security issues and pertinent security information.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                _buildImage('active.png', 100),            ],
            ),
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),

              Text("You will respond to this alerts.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya')),
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),

              Container(
                height: MediaQuery.of(context).size.width*0.25,
                width: MediaQuery.of(context).size.width*0.8,
                decoration: BoxDecoration(
                  color: mainColorrr,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height*0.05,
                      child: Image.asset(
                        'assets/danger.png',
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                      ),

                    ) ,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Safe       ",
                              ),
                              WidgetSpan(
                                child: _buildImage('shield.png', 35),
                              ),
                            ],
                          ),
                        ),
                        Container(height: 20, color: Colors.transparent),

                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Unsafe       ",
                              ),
                              WidgetSpan(
                                child: _buildImage('sikopoa.png', 35),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

              ) ,
              Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
              Container(
                height: MediaQuery.of(context).size.width*0.25,
                width: MediaQuery.of(context).size.width*0.8,
                decoration: BoxDecoration(
                  color: mainColorrr,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height*0.05,
                      child: Image.asset(
                        'assets/information.png',
                        fit: BoxFit.contain,
                        alignment: Alignment.center,
                      ),

                    ) ,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Acknowledge       ",
                              ),
                              WidgetSpan(
                                child: Icon(Icons.check, color: Colors.green, size: 35),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

              ) ,

              Container(height: MediaQuery.of(context).size.height*0.25, color: Colors.transparent),

            ],
          ),


        ),
        PageViewModel(
    titleWidget:SafeArea(
    child: Column(

    children: [
    Container(height: MediaQuery.of(context).size.width*0.1, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,

        children: <Widget>[



              Flexible(
                  child: new Text("Whenever in trouble, use the emergency contact information.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
    ],
    ),
      ],
    ),
    ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Container(
                height: MediaQuery.of(context).size.width*0.3,
                width: MediaQuery.of(context).size.width*0.3,
                decoration: BoxDecoration(
                  color: nyekundu,
                  borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,
                      bottomRight: Radius.circular(20)),
                  border: Border.all(color: mainColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black,
                      blurRadius: 2.0,
                      spreadRadius: 0.0,
                      offset: Offset(2.0, 2.0), // shadow direction: bottom right
                    )
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.add_call, color: nyeupe,),
                    Text("Emergency", style: TextStyle(color: nyeupe)),
                    Text("Contact", style: TextStyle(color: nyeupe)),
                  ],
                ),

              ) ,
              Container(height: MediaQuery.of(context).size.width*0.05, color: Colors.transparent),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[



                  Flexible(
                      child: new Text("This will automatically call us directly and send your location to Insights Regional Operations Center.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                ],
              ),
              Align(
                alignment: Alignment.centerRight,
                child:  _buildImage('dunia.png', 180),
              ),


            ],
          ),


        ),
        PageViewModel(
          titleWidget:Align(
            alignment: Alignment.topCenter,
            child:  _buildImage('insightg.jpg',100)
          ),

          bodyWidget:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[



                  Flexible(
                      child: new Text("Insights 24 hours Regional Operation Center (ROC) is manned by security professionalss who are able to offer advice and coordinate assistance for our clients.", style: TextStyle(fontSize: 35.0, color: nyeupe, fontFamily: 'Microsoft Himalaya'))),
                ],
              ),
              Container(
                height: MediaQuery.of(context).size.height*0.32,
                width:  MediaQuery.of(context).size.height*0.32,
                child: Image(
                  image: AssetImage('assets/akili.png'),
                  alignment: Alignment.center,
                  height: double.infinity,
                  width: double.infinity,
                  fit: BoxFit.fill,
                ),

              )



            ],
          ),

          decoration: pageDecoration.copyWith(
            bodyAlignment: Alignment.center,
            imageAlignment: Alignment.topCenter,
          ),
        ),
        
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      //rtl: true, // Display as right-to-left
      skip: const Text('Skip',  style: TextStyle(color: Colors.red
      )),
      next: const Icon(Icons.arrow_forward, color: nyekundu,),
      done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600, color: Colors.red)),
      curve: Curves.fastLinearToSlowEaseIn,
      controlsMargin: const EdgeInsets.all(16),
      controlsPadding: false
          ? const EdgeInsets.all(12.0)
          : const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeColor:Colors.red,
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        color: mainColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}



